#!/usr/bin/env -S submit -M 3900 -m 7500 -f python

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tabulate import tabulate

#
# Fully connected network example for the MNIST classification task.
# Run this script with 'pygpu %file' in the code editor.
#

# ---------------------------------------------------------
# Load and preprocess CIFAR-10 data
# ---------------------------------------------------------
# X: images, Y: labels
# split 2000 images from test data for validation
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
x_train = x_train[..., np.newaxis]
x_valid = x_test[8000:][..., np.newaxis]
y_valid = y_test[8000:]
x_test = x_test[:8000][..., np.newaxis]
y_test = y_test[:8000]
x_train, x_valid, x_test = x_train / 255.0, x_valid / 255.0, x_test / 255.0


print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_valid.shape[0], "validation samples")
print(x_test.shape[0], "test samples")


# convert class vectors to binary class matrices (10 numbers/classes)
y_train_onehot = tf.keras.utils.to_categorical(y_train, 10)
y_valid_onehot = tf.keras.utils.to_categorical(y_valid, 10)
y_test_onehot = tf.keras.utils.to_categorical(y_test, 10)

# Define neural network

model = tf.keras.models.Sequential(name="mnist")
model.add(tf.keras.layers.Convolution2D(32, kernel_size=(3, 3), activation="relu", input_shape=(28, 28, 1)))
model.add(tf.keras.layers.Convolution2D(64, (3, 3), activation="relu"))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(tf.keras.layers.Dropout(0.25))
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(128, activation="relu"))
model.add(tf.keras.layers.Dropout(0.5))
model.add(tf.keras.layers.Dense(10, activation="softmax"))

model.compile(
    loss="categorical_crossentropy",
    optimizer=tf.keras.optimizers.Adadelta(),
    metrics=["accuracy"],
)

# define callbacks for training
save_best = tf.keras.callbacks.ModelCheckpoint(
    "best_model_{}.h5".format(model.name),
    save_best_only=True,
    monitor="val_accuracy",
    save_weights_only=True,
)

# Keras calculates training accuracy and loss during the training and with regularization applied,
# while the validation metrics are calculated at the end of each epoch.
# This callback calculates the training metrics the same way as for the validation


class CalculateMetrics(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        train_loss, train_acc = model.evaluate(x_train, y_train_onehot, verbose=0)
        logs["train_loss"] = train_loss
        logs["train_acc"] = train_acc


# perform training
model.fit(
    x_train,
    y_train_onehot,
    batch_size=128,
    epochs=20,
    verbose=2,
    validation_data=(x_valid, y_valid_onehot),
    callbacks=[
        save_best,
        CalculateMetrics(),
        tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name)),
    ],
)
# load best model
model.load_weights("best_model_{}.h5".format(model.name))

# ------------------------
# ---     Plotting     ---
# ------------------------

# plot training history
history = np.genfromtxt("history_{}.csv".format(model.name), delimiter=",", names=True)

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["train_loss"], label="training")
ax.plot(history["epoch"], history["val_loss"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="loss")
fig.savefig("loss_{}.png".format(model.name))

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["train_acc"], label="training")
ax.plot(history["epoch"], history["val_accuracy"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="accuracy")
fig.savefig("accuracy_{}.png".format(model.name))

# evaluate performance
print("Model performance :")
headers = ["", "Loss", "Accuracy"]

table = [
    ["Train", *model.evaluate(x_train, y_train_onehot, verbose=0, batch_size=128)],
    ["Validation", *model.evaluate(x_valid, y_valid_onehot, verbose=0, batch_size=128)],
    ["Test", *model.evaluate(x_test, y_test_onehot, verbose=0, batch_size=128)],
]

print(tabulate(table, headers=headers, tablefmt="orgtbl"))

# get missidentified samples
output = model.predict(x_valid, batch_size=128)
predictions = np.argmax(output, axis=1)
labels = np.argmax(y_valid_onehot, axis=1)
indices_miss = np.nonzero(predictions != labels)[0]
x_missid = x_valid[indices_miss]

# plot some of these samples
plt.figure(figsize=(12, 6))
pred_wrong = predictions[indices_miss][:32]
plotdata = x_missid[:32]

for i, img in enumerate(plotdata):
    ax = plt.subplot(4, 8, 1+i, )
    plt.title("prediction % i" % pred_wrong[i])
    plt.imshow(img, cmap="gray")
    ax.axis('off')

plt.tight_layout()
plt.savefig("missidentified_{}.png".format(model.name), dpi=100)
