# -*- coding: utf-8 -*-

# This is a simple, one-file example for setting up a pxl module chain.
# The chain contains an input module and a custom python analyse module.
# Here, we plot the distribution of transverse momentum of leptons
# in Z -> ll decays.

# imports
import os
from ROOT import TCanvas, TH1F
from pxl.core import toEvent
from pxl.modules import Analysis, PythonModule


# define some global variables
THISFILE = os.path.abspath(__file__)
THISDIR  = os.path.dirname(THISFILE)
DATADIR  = "/home/public/CERN/examples/data"


# define a class that is used by our custom analyse module
class AnalyseModule(PythonModule):

    def __init__(self):
        PythonModule.__init__(self)

        # book a histogram
        binning = (50, 0., 100.)
        self.hist_pt = TH1F("hist_pt", ";p_{T,lepton} [GeV/c];# leptons", *binning)

    def analyse(self, obj):
        """ this method is called for every event in the input file """

        # convert the passed object to a pxl event
        event = toEvent(obj)

        # loop through all particles and store the transverse momentum of leptons
        for particle in event.getParticles():
            if particle.getName() == "Lepton":
                self.hist_pt.Fill(particle.getPt())

    def endJob(self):
        """ this method is called once after all events were processed """

        # book a canvas, draw and save
        canvas = TCanvas("canvas", "Canvas", 1024, 768)
        canvas.cd()

        self.hist_pt.Draw()

        canvas.Update()
        canvas.SaveAs(os.path.join(THISDIR, "lepton_pt.png"))


# entry hook
if __name__ == "__main__":
    # reset the __name__ variable to avoid recursive calls
    __name__ = None

    # create an empty analysis
    a = Analysis()

    # add and configure the input module
    a.addModule("File Input", "Input", "0")
    a.setModuleOption("Input", "File names", os.path.join(DATADIR, "Parsed_Z_ll.pxlio"))

    # add and configure the analyse module
    a.addModule("PyAnalyse", "Analyse", "0")
    a.setModuleOption("Analyse", "filename", THISFILE)
    a.setModuleOption("Analyse", "class", AnalyseModule.__name__)

    # connect the two modules
    a.connectModules("Input", "out", "Analyse", "in")

    # finally, run our analysis
    a.run()
