import numpy as np
import warnings
warnings.filterwarnings('ignore',category=FutureWarning)
import tensorflow as tf
import seaborn as sns
import matplotlib.pyplot as plt


def resolution(y_true, y_pred):
    """ Metric to control for standart deviation """
    mean, var = tf.nn.moments((y_true - y_pred), axes=[0])
    return tf.sqrt(var)


def bias(y_true, y_pred):
    """ Metric to control for standart deviation """
    mean, var = tf.nn.moments((y_true - y_pred), axes=[0])
    return mean


def MeanElong(y_true, y_pred):
    """ Metric to control the mean of the angulardistance distribution """
    return 180 / np.pi * tf.reduce_mean(
        tf.minimum(
            tf.acos(
                (
                    y_pred[:, 0] * y_true[:, 0]
                    + y_pred[:, 1] * y_true[:, 1]
                    + y_pred[:, 2] * y_true[:, 2]
                )
                / (
                    tf.sqrt(
                        y_pred[:, 0] * y_pred[:, 0]
                        + y_pred[:, 1] * y_pred[:, 1]
                        + y_pred[:, 2] * y_pred[:, 2]
                    )
                    * tf.sqrt(
                        y_true[:, 0] * y_true[:, 0]
                        + y_true[:, 1] * y_true[:, 1]
                        + y_true[:, 2] * y_true[:, 2]
                    )
                )
            ),
            1,
        )
    )


def Percentile68(x):
    """ Calculates 68% quantile of distribution"""
    return np.percentile(x, 68)


def vec2ang(v):
    """Calculates phi and theta for given shower axis"""
    x, y, z = np.asarray(v).T
    phi = np.arctan2(y, x)
    theta = np.pi / 2 - np.arctan2(z, (x ** 2 + y ** 2) ** 0.5)
    return np.rad2deg(phi), np.rad2deg(theta)


def plot_footprint(data):
    from matplotlib.collections import PatchCollection
    from matplotlib.colors import Normalize

    def rectangular_array(n=9):
        """ Return x,y coordinates for rectangular array with n^2 stations. """
        n0 = (n - 1) / 2
        return (np.mgrid[0:n, 0:n].astype(float) - n0)

    idx = np.random.randint(0, 200000)
    core = data["showercore"][idx]
    logS = np.log10(data["signal"][idx]).flatten()
    time = data["time"][idx].flatten() * 10E6
    axis = data["showeraxis"][idx]
    time[np.isnan(time)] = -10000
    logS[np.isnan(logS)] = -10000
    energy = 10**(data["logE"][idx] - 18)
    x_d, y_d = rectangular_array()
    circles = [plt.Circle((x, y), 0.450) for x, y in zip(x_d.flat, y_d.flat)]
    fig, ax = plt.subplots(1, 2, figsize=(10, 4))

    def plot_array(ax, val, label="logS", vmin=-1, vmax=1):
        coll = PatchCollection(circles, norm=Normalize(vmin=vmin, vmax=vmax))
        mask = ~np.isnan(val)
        coll.set_array(val)
        coll.cmap.set_under('#d3d3d3')
        ax.add_collection(coll)
        ax.grid(True)
        ax.set_aspect('equal')
        ax.set_xlim(-5, 5)
        ax.set_ylim(-5, 5)
        ax.set_xlabel('x / km')
        ax.set_ylabel('y / km')
        # plot shower direction and core
        x, y, z = core / 1000 - 3 * axis
        dx, dy, dz = 3 * axis
        ax.arrow(x, y, dx, dy, lw=2, head_width=0.4, head_length=0.5, fc='r', ec='r')
        cbar = fig.colorbar(coll, ax=ax)
        cbar.set_label(label)

    plot_array(ax[0], logS, "$log_10(S)$", vmin=1, vmax=5)
    plot_array(ax[1], time, "$arrival\;time\;/\;\mu s$", vmin=-230, vmax=230)
    phi, theta = vec2ang(axis)
    fig.suptitle(r"$E = %.1f\;\mathrm{EeV},\; \theta = %.1f°,\; \phi = %.1f°$" % (energy, theta, phi))

    fig.subplots_adjust(left=0.02, top=0.95)
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    fig.savefig("footprint.png")


def plot_xmax_reconstruction(y_true, y_pred, log_dir, energy=None):
    """ Plottingfunction to evaluate the reconstruction of the shower depths Xmax
        plots: predicted Xmax against MC Xmax and the energy dependence of the reconstruction (embedded)"""
    y_true = y_true.reshape(y_true.shape[0], 1)
    reco = y_true - y_pred
    fig, ax1 = plt.subplots(1)
    ax1.scatter(y_true, y_pred, s=2, color="royalblue", alpha=0.90)
    ax1.set_xlabel(r"$X_{max,\;true}$  $[g/cm^{2}]$")
    ax1.set_ylabel(r"$X_{max,\;DNN}$  $[g/cm^{2}]$")
    left, bottom, width, height = [0.23, 0.6, 0.3, 0.26]
    ax1.text(
        0.95,
        0.2,
        r"$\mu = $ %.3f " % np.mean(reco)
        + r"$[g/cm^{2}]$"
        + "\n"
        + "$\sigma = $ %.3f " % np.std(reco)
        + r"$[g/cm^{2}]$",
        verticalalignment="top",
        horizontalalignment="right",
        transform=ax1.transAxes,
        backgroundcolor="w",
    )
    ax1.plot(
        [np.min(y_true), np.max(y_true)], [np.min(y_true), np.max(y_true)], color="red"
    )
    # energy dependence plot
    ax2 = fig.add_axes([left, bottom, width, height])
    x_bins = 12
    if energy is not None:
        ax2 = sns.regplot(
            x=energy,
            y=reco,
            x_estimator=np.std,
            x_bins=x_bins,
            fit_reg=False,
            color="royalblue",
        )
        ax2.tick_params(axis="both", which="major")
        ax2.set_xlabel(r"$E\;/\;\mathrm{EeV}]$")
        ax2.set_ylabel(r"$\sigma_{Xmax}$" + "  " + r"$[g/cm^{2}]$")
        ax2.set(xscale="log")
        x = [3, 10, 30, 100]
        labels = ["$3$", "$10$", "$30$", "$100$"]
        plt.xticks(x, labels)
    fig.savefig(log_dir + "/Xmax_embedding.png")
    return reco


def plot_energy_reconstruction(y_true, y_pred, name):
    """ Plottingfunction to evaluate the reconstruction of the cosmic ray energy
        plots: predicted energy against MC energy and the energy dependence of the relative energy resolution (embedded)"""
    y_true = y_true.reshape(y_true.shape[0], 1)
    reco = y_true - y_pred
    # Embedding plot
    fig, ax1 = plt.subplots(1)
    ax1.scatter(y_true, y_pred, s=2, color="royalblue", alpha=0.90)
    ax1.set_xlabel(r"$E_{true}\;/\;\mathrm{EeV}$")
    ax1.set_ylabel(r"$E_{DNN}\;/\;\mathrm{EeV}$")
    left, bottom, width, height = [0.23, 0.6, 0.3, 0.26]  # Links Oben
    ax1.text(
        0.95,
        0.2,
        r"$\mu = $ %.3f" % np.mean(reco)
        + " / EeV"
        + "\n"
        + "$\sigma = $ %.3f" % np.std(reco)
        + " / EeV",
        verticalalignment="top",
        horizontalalignment="right",
        transform=ax1.transAxes,
        backgroundcolor="w",
    )
    ax1.plot(
        [np.min(y_true), np.max(y_true)], [np.min(y_true), np.max(y_true)], color="red"
    )
    ax2 = fig.add_axes([left, bottom, width, height])
    x_bins = 12
    ax2 = sns.regplot(
        x=y_true,
        y=reco / y_true,
        x_estimator=np.std,
        x_bins=x_bins,
        fit_reg=False,
        color="royalblue",
    )
    ax2.tick_params(axis="both", which="major")
    ax2.set(xscale="log")
    x = [3, 10, 30, 100]
    labels = ["$3$", "$10$", "$30$", "$100$"]
    plt.xticks(x, labels)
    ax2.set_xlabel(r"$E\;/\;\mathrm{EeV}$")
    ax2.set_ylabel(r"$\sigma_{E}/E$")
    fig.savefig(name)
    return reco


def plot_angular_reconstruction(y_true, y_pred, energy=None):
    """ Plotting function to evaluate the reconstruction of the shower axis
        plots: angular distance distribution and the energy dependence of the angular reconstruction (embedded)"""
    phi, theta = vec2ang(y_true)
    angulardistance = (
        180
        / np.pi
        * np.arccos(
            np.clip(
                np.sum(y_true * y_pred, axis=-1)
                / np.linalg.norm(y_pred, axis=-1)
                / np.linalg.norm(y_true, axis=-1),
                -1,
                1,
            )
        )
    )
    resolution = Percentile68(angulardistance)
    # Embedding plot
    fig, ax1 = plt.subplots(1)
    ax1.hist(angulardistance, bins=np.linspace(0, 4, 51))
    ax1.set(xlabel=r"$Angulardistance\;\delta\;[^\circ]$", ylabel=r"$N$")
    left, bottom, width, height = [0.56, 0.6, 0.31, 0.27]
    ax2 = fig.add_axes([left, bottom, width, height])
    x_bins = 12
    if energy is not None:
        ax2 = sns.regplot(
            x=energy,
            y=angulardistance,
            x_estimator=Percentile68,
            x_bins=x_bins,
            fit_reg=False,
            color="royalblue",
        )
        ax2.tick_params(axis="both", which="major")
        ax2.set(xscale="log")
        x = [3, 10, 30, 100]
        labels = ["$3$", "$10$", "$30$", "$100$"]
        plt.xticks(x, labels)
        ax2.set_xlabel(r"$E\;/\;\mathrm{EeV}$")
        ax2.set_ylabel(r"$Resolution\;[^\circ]$")
    fig.savefig("Ang_Res_embedding.png")
    return angulardistance
