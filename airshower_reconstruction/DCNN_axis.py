#!/usr/bin/env pygpu
"""
Advanced Convolutional network for the Air Shower reconstruction at an "Auger like observatory".
Run this script with "pygpu %file" in the code editor or "pygpu DNN_Xmax.py" in the terminal.
https://doi.org/10.1016/j.astropartphys.2017.10.006
"""
import numpy as np
import tools
import tensorflow as tf
import matplotlib.pyplot as plt


# ---------------------------------------------------
# Load and prepare dataset - you can leave as is.
# ---------------------------------------------------
data = np.load("/net/scratch/deeplearning/airshower/auger-shower-planar.npz")

tools.plot_footprint(data)  # plot example footprint

# time map, values standard normalized with untriggered stations set to 0
T = data["time"]
T -= np.nanmean(T)
T /= np.nanstd(T)
T[np.isnan(T)] = 0

# signal map, values normalized to range 0-1, untriggered stations set to 0
S = data["signal"]
S = np.log10(S)
S -= np.nanmin(S)
S /= np.nanmax(S)
S[np.isnan(S)] = 0

# input features
X = np.stack([T, S], axis=-1)

# energy - E in EeV [0-100 EeV]
logE = data["logE"]
energy = 10 ** (logE - 18)

# target features
# direction - x,y,z unit vector
axis = data["showeraxis"]

# hold out the last 20000 events as test data
x_train, x_test = np.split(X, [-20000])
axis_train, axis_test = np.split(axis, [-20000])
energy, energy_test = np.split(energy, [-20000])

# ----------------------------------------------------------------------
# Advanced Computer Vision modules
# ----------------------------------------------------------------------


def densely_connected_separable_convolution(z, nfilter, **kwargs):
    " Densely Connected SeparableConvolution2D Layer"
    c = tf.keras.layers.SeparableConv2D(nfilter, (3, 3), padding="same", depth_multiplier=1, **kwargs)(z)
    return tf.keras.layers.concatenate([z, c], axis=-1)


def inception_unit(x0):
    x1 = tf.keras.layers.Convolution2D(16, (1, 1), padding="same", activation="relu")(x0)
    x1 = tf.keras.layers.Convolution2D(16, (3, 3), padding="same", activation="relu")(x1)

    x2 = tf.keras.layers.Convolution2D(16, (1, 1), padding="same", activation="relu")(x0)
    x2 = tf.keras.layers.Convolution2D(16, (5, 5), padding="same", activation="relu")(x2)

    x3 = tf.keras.layers.Convolution2D(16, (1, 1), padding="same", activation="relu")(x0)

    x4 = tf.keras.layers.MaxPooling2D((3, 3), strides=(1, 1), padding="same")(x0)
    x4 = tf.keras.layers.Convolution2D(64, (1, 1), padding="same", activation="relu")(x4)
    return tf.keras.layers.concatenate([x1, x2, x3, x4], axis=-1)


def residual_unit(x0, nfilter=64):
    x0 = tf.keras.layers.Convolution2D(nfilter, (1, 1), padding="same")(x0)  # bottleneck layer
    x = tf.keras.layers.Convolution2D(nfilter, (3, 3), padding="same")(x0)
    x = tf.keras.layers.Activation("relu")(x)
    x = tf.keras.layers.Convolution2D(nfilter, (3, 3), padding="same")(x)
    return tf.keras.layers.add([x, x0])


# ----------------------------------------------------------------------
# Model
# ----------------------------------------------------------------------

inputs = tf.keras.layers.Input(shape=x_train.shape[1:])
kwargs = dict(activation="relu", kernel_initializer="he_normal")
x = tf.keras.layers.Convolution2D(16, (3, 3), padding="same", **kwargs)(inputs)
x = tf.keras.layers.Convolution2D(16, (3, 3), padding="same", **kwargs)(x)

# Densely connected convolutions
nfilter = 16
for i in range(4):
    x = densely_connected_separable_convolution(x, nfilter)
    nfilter = 2 * nfilter

x = tf.keras.layers.SeparableConv2D(
    nfilter, (3, 3), padding="same", depth_multiplier=1, **kwargs
)(x)
x = tf.keras.layers.Flatten()(x)
outputs = tf.keras.layers.Dense(3)(x)

model = tf.keras.models.Model(
    name="axis_regression_DCNN", inputs=inputs, outputs=outputs
)
print(model.summary())

# plot float chart of your model
tf.keras.utils.plot_model(
    model, to_file="{}.png".format(model.name), show_shapes=True
)
# ----------------------------------------------------------------------

model.compile(
    loss="mse",                                    # objective function which is minimized during training
    optimizer=tf.keras.optimizers.Adam(lr=0.001),  # optimization algorithm, lr=learning rate
    metrics=[tools.resolution, tools.bias]         # additional metrics helping to monitor the DNN performance
)

#  callback reduces learning rate for later stages of the training
#  multiply learning rate with "factor", if after "patience" iterations no improvement of "min_delta" was reached
fit = model.fit(
    x_train,               # input data for the neural network
    axis_train,            # dataset labels
    batch_size=128,        # number of samples used for one update of the weights and biases
    epochs=40,             # number of iterations over the whole dataset
    verbose=2,             # print one line per epoch
    validation_split=0.1,  # use 10 percent of the data for the validation
    callbacks=[
        tf.keras.callbacks.ReduceLROnPlateau(
            monitor="val_loss",
            factor=0.5,
            patience=3,
            verbose=1,
            mode="auto",
            min_delta=0,
            min_lr=1e-5,
        ),
        tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name)),
    ],
)


# ----------------------------------------------------------------------
# Evaluation
# ----------------------------------------------------------------------
history = np.genfromtxt("history_{}.csv".format(model.name), delimiter=",", names=True)

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["loss"], label="training")
ax.plot(history["epoch"], history["val_loss"], label="validation")
ax.legend()
ax.set_yscale("log", nonposy="clip")
ax.set(xlabel="epoch", ylabel="loss")
fig.savefig("loss_{}.png".format(model.name))

y_pred = model.predict(x_test)
angulardistance = tools.plot_angular_reconstruction(
    y_true=axis_test, y_pred=y_pred, energy=energy_test
)
print("mean", np.mean(angulardistance), "std", np.std(angulardistance))
