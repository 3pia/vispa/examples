#!/usr/bin/env pygpu

import numpy as np
import tools
import tensorflow as tf
import matplotlib.pyplot as plt
layers = tf.keras.layers


# ---------------------------------------------------
# Load and prepare dataset - you can leave as is.
# ---------------------------------------------------
data = np.load("/net/scratch/deeplearning/airshower/auger-shower-planar.npz")

tools.plot_footprint(data)  # plot example footprint

# time map, values standard normalized with untriggered stations set to 0
T = data["time"]
T -= np.nanmean(T)
T /= np.nanstd(T)
T[np.isnan(T)] = 0

# signal map, values normalized to range 0-1, untriggered stations set to 0
S = data["signal"]
S = np.log10(S)
S -= np.nanmin(S)
S /= np.nanmax(S)
S[np.isnan(S)] = 0

# input features
X = np.stack([T, S], axis=-1)

# energy - E in EeV [0-100 EeV]
logE = data["logE"]
energy = 10 ** (logE - 18)

# hold out the last 20000 events as test data
x_train, x_test = np.split(X, [-20000])
energy_train, energy_test = np.split(energy, [-20000])

# ----------------------------------------------------------------------
# Model
# ----------------------------------------------------------------------

model = tf.keras.models.Sequential(name="energy_regression_NN")
model.add(layers.Flatten(input_shape=x_train.shape[1:]))
model.add(layers.Dense(256, activation="relu"))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(256, activation="relu"))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(256, activation="relu"))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(1))

print(model.summary())

# plot float chart of your model
tf.keras.utils.plot_model(
    model, to_file="{}.png".format(model.name), show_shapes=True
)
# ----------------------------------------------------------------------

model.compile(
    loss="mse",                                    # objective function which is minimized during training
    optimizer=tf.keras.optimizers.Adam(lr=0.001),  # optimization algorithm, lr=learning rate
    metrics=[tools.resolution, tools.bias]         # additional metrics helping to monitor the DNN performance
)

#  callback reduces learning rate for later stages of the training
#  multiply learning rate with "factor", if after "patience" iterations no improvement of "min_delta" was reached
fit = model.fit(
    x_train,               # input data for the neural network
    energy_train,          # dataset labels
    batch_size=128,        # number of samples used for one update of the weights and biases
    epochs=40,             # number of iterations over the whole dataset
    verbose=2,             # print one line per epoch
    validation_split=0.1,  # use 10 percent of the data for the validation
    callbacks=[
        tf.keras.callbacks.ReduceLROnPlateau(
            monitor="val_loss",
            factor=0.5,
            patience=3,
            verbose=1,
            mode="auto",
            min_delta=0,
            min_lr=1e-5,
        ),
        tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name)),
    ],
)


# ----------------------------------------------------------------------
# Evaluation
# ----------------------------------------------------------------------
history = np.genfromtxt("history_{}.csv".format(model.name), delimiter=",", names=True)

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["loss"], label="training")
ax.plot(history["epoch"], history["val_loss"], label="validation")
ax.legend()
ax.set_yscale("log", nonposy="clip")
ax.set(xlabel="epoch", ylabel="loss")
fig.savefig("loss_{}.png".format(model.name))

y_pred = model.predict(x_test)

reco = tools.plot_energy_reconstruction(y_true=energy_test, y_pred=y_pred,
                                        name="reco_{}.png".format(model.name))

print("mean", np.mean(reco), "std", np.std(reco))
