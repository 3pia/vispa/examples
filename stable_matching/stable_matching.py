import os
from matplotlib.patches import Circle
import matplotlib.pyplot as plt
import matplotlib

##################################
# Algorithm to calculate a stable matching between two independent sets,
# here denotes as men and women, with individual preference lists.
# The actual algorithm is implemented in the main method at the end of this file.
# The definition of the two sets and their preferences are defined at the beginning
# of the main method.
##################################

PLOT_PATH = ""
PLOT_COUNTER = 0
r = 0.7
dy = 2.5
x_a, x_b = 1, 10
y_start = 10


class Man():  # men
    def __init__(self, id, preference_ids):
        self.id = id
        self.p = []
        self.r = []  # rejection history
        self.state = 0
        self.i = 0  # current index of proposal
        self.preference_ids = preference_ids

    def accept(self, b):
        self.p.append(b)

    def get_number_of_proposals(self):
        return len(self.p)

    def has_given_up(self):
        return self.state == 3

    def get_number_of_women(self):
        return len(self.preference_ids)

    def is_fully_rejected(self):
        for womenid in self.preference_ids:
            if womenid not in self.r:
                return False
        return True

    def get_women_id(self):
        return self.preference_ids[self.i]


class Woman():  # women
    def __init__(self, id, preference_ids, ranks):
        self.r = []  # rejection history
        self.accepted_proposals = []
        self.id = id
        self.preference_list = {}
        for id, rank in zip(preference_ids, ranks):
            self.preference_list[id] = rank

    def _rank(self, a):
        return self.preference_list[a.id]

    def _compare_two(self, a, b):
        if(self._rank(a) > self._rank(b)):
            return True
        if(self._rank(a) < self._rank(b)):
            return False
        if(self._rank(a) == self._rank(b)):
            if(a.state != b.state):  # if ranks are equal select higher promoted man
                print("state a %i: %i" % (a.id, a.state))
                print("state b %i: %i" % (b.id, b.state))
                if(a.state == 3):  # if one of the men has already given up, rate this man lowest
                    return False
                elif(b.state == 3):
                    return True
                else:
                    return a.state > b.state
            else:
                if(a.id in self.r):
                    return True
                elif(b.id in self.r):
                    return False
                else:
                    print("the two men are equal, rejecting new proposal")
                    return False

    def reject_least_desirable_proposal(self, a1):
        if(len(self.accepted_proposals) != 2):
            print("error, less than two accapted proposals")
            raise
        a2 = self.accepted_proposals[0]
        a3 = self.accepted_proposals[1]

        if(a2.id == a3.id):
            print("the two accepted proposals are equal")
            if(self._compare_two(a1, a2)):  # a is ranked higher than b and c
                print("man %i is ranked higher than man %i" % (a1.id, a2.id))
                self.accepted_proposals[1] = a1
                self.r.append(a3.id)
                a1.p.append(self)
                a3.r.append(self.id)
                a3.p.remove(self)
                return a3
            else:
                print("man %i is ranked higher than man %i" % (a2.id, a1.id))
                a1.r.append(self.id)
                return a1

        lowest_ranked_promoted_id = 0
        if(self._compare_two(a2, a3)):  # b is ranked higher than c
            print("man %i is ranked higher than man %i" % (a1.id, a2.id))
            lowest_ranked_promoted_id = 1
        lowest_ranked_promoted_man = self.accepted_proposals[lowest_ranked_promoted_id]
        print("man %i of the two accepted proposals is rated worse" % (lowest_ranked_promoted_man.id))

        if(self._compare_two(a1, lowest_ranked_promoted_man)):  # compare promoter with lowest ranked current promoted man
            print("man %i is ranked higher than man %i" % (a1.id, lowest_ranked_promoted_man.id))
            self.accepted_proposals[lowest_ranked_promoted_id] = a1
            self.r.append(lowest_ranked_promoted_man.id)
            a1.p.append(self)
            lowest_ranked_promoted_man.r.append(self.id)
            lowest_ranked_promoted_man.p.remove(self)
            return lowest_ranked_promoted_man
        else:
            print("man %i is ranked higher than man %i" % (lowest_ranked_promoted_man.id, a1.id))
            a1.r.append(self.id)
            return a1


def plot_edge(ax, a, b, offset=0):
    return ax.plot([1, 10], [10 - a.id * dy + offset, 10 - b.id * dy + offset],
                   "-b", zorder=-2, alpha=0.4)


def plot_edges(ax, A):
    edges = []
    for a in A:
        if(len(a.p) == 1):
            o, = plot_edge(ax, a, a.p[0], -0.1)
            edges.append(o)
        if(len(a.p) == 2):
            if(a.p[0].id == a.p[1].id):
                o, = plot_edge(ax, a, a.p[0], -0.1)
                edges.append(o)
                o, = plot_edge(ax, a, a.p[1], 0.1)
                edges.append(o)
            else:
                o, = plot_edge(ax, a, a.p[0])
                edges.append(o)
                o, = plot_edge(ax, a, a.p[1])
                edges.append(o)
    return edges


def plot_state(ax, a):
    return ax.text(1, 10 - a.id * dy - r - 0.1, STATES[a.state], ha="center", size="small", va="top")


def plot_states(ax, A):
    states = []
    for a in A:
        states.append(plot_state(ax, a))
    return states


def plot_preferences(ax, A, B):
    names_men = ["Anton", "Bob", "Clemens", "Dominik"]
    names_women = ["Anna", "Bea", "Christine", "Dora"]
    for j, a in enumerate(A):
        txt = ""
        for i in a.preference_ids:
            txt += "b%i, " % i
        ax.text(x_a - r - 0.1, 10 - a.id * dy, txt[:-2], ha="right", size="small", va="center")
        ax.text(x_a - r - 5, 10 - a.id * dy, names_men[j], fontweight="heavy", ha="left", size="large", va="center")
    for i, b in enumerate(B):
        p = b.preference_list
        if(len(p) != 0):
            txt = ""
            import operator
            sorted_p = sorted(p.items(), key=operator.itemgetter(1), reverse=True)
            print(sorted_p)
            first_key, first_value = sorted_p[0]
            txt += "a%i" % first_key
            for key, value in sorted_p[1:]:
                if(value == first_value):
                    txt += " a%i" % key
                else:
                    txt += ", a%i" % key
                    first_value = value
            ax.text(x_b + r + 0.1, 10 - b.id * dy, txt, ha="left", size="small", va="center")
        ax.text(x_b - r + 5, 10 - b.id * dy, names_women[i], ha="left", size="large", va="center", fontweight="heavy")


def plot_basis(ax, N_A, N_B):
    text_offset_x, text_offset_y = -0.3, -0.2
    y1 = y_start
    for i in range(N_A):
        circle_bg = Circle((x_a, y1), r, color="w", alpha=1, zorder=-1)
        ax.add_patch(circle_bg)
        circle = Circle((x_a, y1), r, facecolor="b", alpha=0.4)
        ax.add_patch(circle)
        ax.text(x_a + text_offset_x, y1 + text_offset_y, "a%i" % i)
        y1 += -dy
    y1 = y_start
    for i in range(N_B):
        circle_bg = Circle((x_b, y1), r, color="w", alpha=1, zorder=-1)
        ax.add_patch(circle_bg)
        circle = Circle((x_b, y1), r, facecolor="r", alpha=0.4)
        ax.add_patch(circle)
        ax.text(x_b + text_offset_x, y1 + text_offset_y, "b%i" % i)
        y1 += -dy


def plot_highlight_man(ax, a):
    circle = Circle((x_a, 10 - a.id * dy), r, edgecolor="k", facecolor="none", lw=2, alpha=1, zorder=1)
    ax.add_patch(circle)
    return circle


def plot_propose(ax, a, b):
    offset = 0
    if(len(a.p) == 1):
        if(a.p[0].id == b.id):
            offset = +0.1
    o1, = ax.plot([1, 10], [10 - a.id * dy + offset, 10 - b.id * dy + offset],
                  "-b", zorder=-2, alpha=0.9, color="g", ls="--")
    o2 = ax.text(5, (10 - a.id * dy + offset + 10 - b.id * dy + offset) * 0.5,
                 "?", color="g", ha="center", size="large", va="bottom", weight="bold")
    return o1, o2


def plot_dashed(ax, a, b):
    offset = 0
    if(len(a.p) == 1):
        if(a.p[0].id == b.id):
            offset = +0.1
    o1, = ax.plot([1, 10], [10 - a.id * dy + offset, 10 - b.id * dy + offset],
                  "-b", zorder=-2, alpha=0.9, color="k", ls="--")
    return o1


def plot_decline(ax, a, b):
    offset = 0
    if(len(a.p) >= 1):
        if(a.p[0].id == b.id):
            offset = +0.1
    y_a = 10 - a.id * dy + offset
    y_b = 10 - b.id * dy + offset
    y = y_a + (y_b - y_a) * 0.5 + 0.2
    o1, = ax.plot([1, 10], [10 - a.id * dy + offset, 10 - b.id * dy + offset],
                  "-b", zorder=-2, alpha=0.9, color="r", ls="--")
    o2 = ax.text(5, y, "X", color="r", ha="center", size="large", va="center", fontweight="extra bold")
    return o1, o2


def plot_accept(ax, a, b):
    offset = 0
    if(len(a.p) > 1):
        if(a.p[0].id == b.id):
            offset = +0.1
    y_a = 10 - a.id * dy + offset
    y_b = 10 - b.id * dy + offset
    y = y_a + (y_b - y_a) * 0.5
    o1, = ax.plot([1, 10], [10 - a.id * dy + offset, 10 - b.id * dy + offset],
                  "-b", zorder=-2, alpha=1, color="g", ls="-")
    o2 = ax.text(5, y, "Ja", color="g", ha="left", size="large", va="bottom", fontweight="extra bold")
    return o1, o2


def plot_save(fig, ax, counter, prefix="", title=""):
    if(title != ""):
        ax.set_title(title)
    else:
        ax.set_title("step %i" % counter)
    global PLOT_COUNTER
    # plt.tight_layout()
    fig.savefig(os.path.join(PLOT_PATH, "%s_%03i.png" % (prefix, PLOT_COUNTER)))
    PLOT_COUNTER += 1


def plot_remove(ax, tt):
    if(tt is not None):
        for t in tt:
            if(type(t) == matplotlib.lines.Line2D):
                ax.lines.remove(t)
            elif(type(t) == matplotlib.text.Text):
                ax.texts.remove(t)
            else:
                t.remove()
    return ax


if __name__ == "__main__":
    A = []  # list of men
    B = []  # list of women

    prefix = "example2"

    #############################
    # CHANGE INITIALIZATION HERE
    #############################
    # define man with preferences
    # The first argument of Man() is the id (has to be unique)
    # the second parameter is a list of its preferences in a strict order
    # in this case man 0 prefers woman 0 over women 2, and woman 2 over women 1
    A.append(Man(0, [0, 2, 1]))
    A.append(Man(1, [0, 1, 2]))
    A.append(Man(2, [0, 1, 2]))
    A.append(Man(3, [0, 2, 1]))


    # define woman with preferences
    # as women can have ties on their preference lists the constructor is different
    # The first argument of Woman() is the id (has to be unique)
    # The second arumgnet is a list with the ids of the men that are on the womans preference list
    # The third argument is a list that ranks the men, a higher number means a higher ranking
    B.append(Woman(0, [0, 1, 2, 3], [1, 2, 3, 2]))
    B.append(Woman(1, [0, 1, 2, 3], [3, 2, 1, 0]))
    B.append(Woman(2, [0, 1, 2, 3], [2, 2, 0, 2]))

    N_A = len(A)
    N_B = len(B)

    STATES = {0: "basic", 1: "1-promoted", 2: "2-promoted", 3: "given up"}

    fig, ax = plt.subplots(1, 1)
    plt.axis('off')
    ax.set_xlim(-2, 15)
    ax.set_ylim(9 - 2 * max(N_A, N_B), 12)
    ax.grid(False)
    ax.set_aspect("equal")
    counter = 0
    plot_basis(ax, N_A, N_B)
    plot_preferences(ax, A, B)
    states = plot_states(ax, A)
    plot_save(fig, ax, counter, prefix)
    plot_remove(ax, states)

    while True:
        do_break = True
        ca = None  # current man
        # check if algorithm has terminated, if not select current man
        for a in A:
            if((a.get_number_of_proposals() < 2) and not a.has_given_up()):
                ca = a
                do_break = False
                break
        if do_break:
            break

        counter += 1
        edges = plot_edges(ax, A)
        states = plot_states(ax, A)
        highlight = [plot_highlight_man(ax, ca)]

        b = B[ca.get_women_id()]  # select current women
        print("\nround %i, man %i proposes to woman %i" % (counter, ca.id, b.id))
        p_proposal = plot_propose(ax, ca, b)
        plot_save(fig, ax, counter, prefix)

        if(len(b.accepted_proposals) < 2):
            b.accepted_proposals.append(ca)
            ca.p.append(b)
            print("woman %i accepts man %i" % (b.id, ca.id))
            plot_remove(ax, p_proposal)
            p_accept = plot_accept(ax, ca, b)
            plot_save(fig, ax, counter, prefix)
            plot_remove(ax, p_accept)

        else:
            ar = b.reject_least_desirable_proposal(ca)

            print("woman %i rejectes man %i" % (b.id, ar.id))
            plot_remove(ax, p_proposal)
            p_reject = plot_decline(ax, ar, b)
            if(ar.id != ca.id):
                print("woman %i accepts man %i" % (b.id, ca.id))
                p_accept = plot_accept(ax, ca, b)

            plot_save(fig, ax, counter, prefix)
            if(ar.id != ca.id):
                plot_remove(ax, p_accept)
            plot_remove(ax, p_reject)

            if(ar.id == ca.id):
                if(ar.i == (ar.get_number_of_women() - 1)):
                    ar.i = 0
                else:
                    ar.i += 1

            ar.r.append(b.id)  # add women b to rejection history of rejeted man ar
            if(ar.is_fully_rejected()):
                ar.state += 1
                ar.r = []
        plot_remove(ax, edges)
        plot_remove(ax, states)
        edges = plot_edges(ax, A)
        states = plot_states(ax, A)
        plot_save(fig, ax, counter, prefix)
        plot_remove(ax, edges)
        plot_remove(ax, states)
        plot_remove(ax, highlight)

    print("results:")
    for a in A:
        print("man %i has women %s" % (a.id, str([x.id for x in a.p])))



