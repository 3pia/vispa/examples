#!/usr/bin/env pygpu

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import utils

# ---------------------------------------------------------
# Load and plot data
# ---------------------------------------------------------
jets, labels = utils.load_data(type="4vectors", data="train")  # load data
print("shape of images", jets.shape)
print("shape of labels", labels.shape)

# ---------------------------------------------------------
# Build your model
# ---------------------------------------------------------
inp = tf.keras.layers.Input(shape=(200, 6, 1))
y = tf.keras.layers.Convolution2D(8, (3, 6), activation="relu", padding="valid")(inp)
y = tf.keras.layers.Convolution2D(16, (3, 1), activation="relu", padding="valid")(y)
y = tf.keras.layers.MaxPooling2D((3, 1))(y)
y = tf.keras.layers.Convolution2D(32, (3, 1), activation="relu", padding="valid")(y)
y = tf.keras.layers.Flatten()(y)
y = tf.keras.layers.Dense(2, activation="softmax")(y)

model = tf.keras.models.Model(inputs=inp, outputs=y, name="four_vectors")
print(model.summary())  # print model details

model.compile(
    loss="sparse_categorical_crossentropy",
    optimizer=tf.keras.optimizers.Adam(0.001),
    metrics=["accuracy"],
)

model.fit(
    jets,
    labels,
    epochs=5,
    validation_split=0.1,
    batch_size=128,
    verbose=2,
    callbacks=[tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name))],
)

# ---------------------------------------------------------
# Plot your losses and results
# ---------------------------------------------------------
history = np.genfromtxt("history_{}.csv".format(model.name), delimiter=",", names=True)

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["loss"], label="training")
ax.plot(history["epoch"], history["val_loss"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="loss")
fig.savefig("loss_{}.png".format(model.name))

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["accuracy"], label="training")
ax.plot(history["epoch"], history["val_accuracy"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="accuracy")
fig.savefig("accuracy_{}.png".format(model.name))

jets_test, truth = utils.load_data(type="4vectors", data="test")  # load test data
truth_top = truth.astype(np.bool)
predictions = model.predict(jets_test)
fig, ax = plt.subplots(1)
ax.hist(
    predictions[:, 1][~truth_top], label="qcd", alpha=0.6, bins=np.linspace(0, 1, 40)
)
ax.hist(
    predictions[:, 1][truth_top], label="top", alpha=0.6, bins=np.linspace(0, 1, 40)
)
ax.set_xlim(0, 1)
ax.legend()
ax.set(xlabel="output", ylabel="#")
fig.savefig("discrimination_{}.png".format(model.name))
