#!/usr/bin/env pygpu

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import utils


# ---------------------------------------------------------
# Load and plot data
# ---------------------------------------------------------
images, labels = utils.load_data(type="images", data="train")  # load data
# utils.plot_average_images(images, labels, folder)
print("shape of images", images.shape)
print("shape of labels", labels.shape)

fig, axes = plt.subplots(1, 2, figsize=(10, 5))
for i, ax in enumerate(axes):
    idx = np.random.choice(np.where(labels == i)[0])
    norm = LogNorm(10 ** -4, images.max(), clip="True")
    im = ax.imshow(images[idx][..., -1], norm=norm)
    ax.set_xlabel("eta")
    ax.set_ylabel("phi")
axes[0].set_title("qcd")
axes[1].set_title("top")
fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
fig.colorbar(im, cax=cbar_ax)
fig.savefig("random_images.png")

# ---------------------------------------------------------
# Build your model
# ---------------------------------------------------------
inp = tf.keras.layers.Input(shape=(40, 40, 1))
y = tf.keras.layers.Convolution2D(8, 3, activation="relu", padding="same")(inp)
y = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(y)
y = tf.keras.layers.Convolution2D(16, 5, activation="relu", padding="same")(y)
y = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(y)
y = tf.keras.layers.Convolution2D(32, 5, activation="relu", padding="same")(y)
y = tf.keras.layers.Flatten()(y)
y = tf.keras.layers.Dropout(0.1)(y)
y = tf.keras.layers.Dense(2, activation="softmax")(y)

model = tf.keras.models.Model(inputs=inp, outputs=y, name="images")
print(model.summary())  # print model details

model.compile(
    loss="sparse_categorical_crossentropy",
    optimizer=tf.keras.optimizers.Adam(0.001),
    metrics=["accuracy"],
)

model.fit(
    images,
    labels,
    epochs=5,
    validation_split=0.1,
    batch_size=128,
    verbose=2,
    callbacks=[tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name))],
)

# ---------------------------------------------------------
# Plot your losses and results
# ---------------------------------------------------------
history = np.genfromtxt("history_{}.csv".format(model.name), delimiter=",", names=True)

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["loss"], label="training")
ax.plot(history["epoch"], history["val_loss"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="loss")
fig.savefig("loss_{}.png".format(model.name))

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["accuracy"], label="training")
ax.plot(history["epoch"], history["val_accuracy"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="acc")
fig.savefig("accuracy_{}.png".format(model.name))

images_test, truth = utils.load_data(type="images", data="test")  # load test data
truth_top = truth.astype(np.bool)
predictions = model.predict(images_test)
fig, ax = plt.subplots(1)
ax.hist(
    predictions[:, 1][~truth_top], label="qcd", alpha=0.6, bins=np.linspace(0, 1, 40)
)
ax.hist(
    predictions[:, 1][truth_top], label="top", alpha=0.6, bins=np.linspace(0, 1, 40)
)
ax.set_xlim(0, 1)
ax.legend()
ax.set(xlabel="output", ylabel="#")
fig.savefig("discrimination_{}.png".format(model.name))
