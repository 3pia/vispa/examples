import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf


def get_data(n, tracelength, n_classes):
    peak = np.array([1, 2, 4, 8, 10, 11, 10, 8, 4, 2, 1]) * 0.1
    traces_1 = np.random.rand(n, tracelength) * 0.1
    traces_2 = np.random.rand(n, tracelength) * 0.1

    common_peaks = n_classes - 1

    labels = np.zeros((n, n_classes))
    peak_positions = np.array([np.random.choice(np.linspace(14, tracelength - 13, num=10, dtype="int"), common_peaks, replace=False) for _ in range(n)])
    peak_positions += np.random.choice(np.arange(-4, 5), peak_positions.shape)

    for (i, peaks) in enumerate(peak_positions):
        peak_mask = np.ones(tracelength)
        peak_mask[:8] = 0
        peak_mask[-8:] = 0

        for pp in peaks:
            traces_1[i, pp - 5:pp + 6] += (peak * (0.7 + 0.3 * np.random.rand(11)))
            pp += np.random.choice(np.arange(-2, 3))
            traces_2[i, pp - 5:pp + 6] += (peak * (0.7 + 0.3 * np.random.rand(11)))
            peak_mask[pp - 7:pp + 8] = 0

        random_peak_position_1 = np.random.choice(np.arange(tracelength)[peak_mask.astype(bool)])
        traces_1[i, random_peak_position_1 - 5:random_peak_position_1 + 6] += (peak * (0.7 + 0.3 * np.random.rand(11)))

        cpeaks_before = np.sum(peaks < random_peak_position_1)
        labels[i, cpeaks_before] = 1

    return traces_1, traces_2, labels


def plot_example_traces(traces_1, traces_2, labels, fname, n_classes):
    plt.figure()
    plt.grid()
    plt.plot(traces_1, label="Trace 1")
    plt.plot(traces_2, label="Trace 2")
    plt.xlabel("Time step")
    plt.ylabel("Signal")
    plt.title("Common peaks before individual peak: %i" % np.arange(n_classes)[labels.astype(bool)])
    plt.legend(loc="upper left")
    plt.savefig(fname, dpi=300)


def get_angles(pos, i, d_model):
    angle_rates = 1 / np.power(10000, (2 * (i // 2)) / np.float32(d_model))
    return pos * angle_rates


def positional_encoding(position, d_model):
    angle_rads = get_angles(np.arange(position)[:, np.newaxis],
                            np.arange(d_model)[np.newaxis, :],
                            d_model)

    # apply sin to even indices in the array; 2i
    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])

    # apply cos to odd indices in the array; 2i+1
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

    pos_encoding = angle_rads[np.newaxis, ...]

    return tf.cast(pos_encoding, dtype=tf.float32)


def plot_training(history):
    steps = np.arange(history.params["epochs"])
    val_loss = history.history["val_loss"]
    val_acc = history.history["val_acc"]

    w, h = plt.figaspect(0.5)
    fig, ax2 = plt.subplots(figsize=(w, h))
    color = 'navy'
    ax2.set_ylabel('Validation Loss', color=color, fontsize=17)  # we already handled the x-label with ax1
    ax2.plot(steps + 1, val_loss, color=color, zorder=2)
    ax2.set_xlabel('Epoch', fontsize=17)
    ax2.set_xlim(1, len(val_loss))
    ax2.tick_params(axis='x', labelsize=15)
    ax2.set_yscale("log")
    ax2.set_ylim(0.5 * np.min(val_loss), 1.1 * np.max(val_loss))
    ax2.tick_params(axis='y', labelcolor=color, labelsize=15)
    ax1 = ax2.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'firebrick'
    ax1.plot(steps + 1, val_acc, color=color, zorder=3)
    ax1.set_ylabel('Validation Accuracy', color=color, fontsize=17)
    ax1.set_ylim(0.0, 1.0)
    ax1.tick_params(axis='y', labelcolor=color, labelsize=15)
    fig.tight_layout()
    plt.savefig("training.png", dpi=300)


def plot_confusion(yp, y, classes=["0", "1", "2", "3"], fname="confusion.png"):
    """Plot confusion matrix for given true and predicted class labels
    Args:
        yp (1D array): predicted class labels
        y (1D array): true class labels
        classes (1D array): class names
        fname (str, optional): filename for saving the plot
    """
    n = len(classes)

    bins = np.linspace(-0.5, n - 0.5, n + 1)
    C = np.histogram2d(y, yp, bins=bins)[0]
    C = C / np.sum(C, axis=0) * 100

    fig = plt.figure(figsize=(8, 8))
    plt.imshow(C, interpolation='nearest', vmin=0, vmax=100, cmap='Greys')
    plt.gca().set_aspect('equal')
    cbar = plt.colorbar(shrink=0.8)
    cbar.set_label('Frequency in %', fontsize=20)
    plt.xlabel('Prediction', fontsize=20)
    plt.ylabel('Truth', fontsize=20)
    plt.xticks(range(n), classes, rotation='vertical', fontsize=20)
    plt.yticks(range(n), classes, fontsize=20)
    for x in range(n):
        for y in range(n):
            if np.isnan(C[x, y]):
                continue
            color = 'white' if x == y else 'black'
            plt.annotate('%.1f' % (C[x, y]), xy=(y, x), color=color, ha='center', va='center', fontsize=20)
    fig.savefig(fname, bbox_inches='tight', dpi=300)
    plt.close()


def plot_attention_scores(model, attention_layer, t1, t2, fname, query_index=-1, key_index=-2):
    a_layer = model.layers[attention_layer]
    x1_layer = model.layers[attention_layer + key_index]
    x2_layer = model.layers[attention_layer + query_index]

    x1_model = tf.keras.models.Model(model.input, x1_layer.output)
    x1 = x1_model.predict([t1, t2])

    x2_model = tf.keras.models.Model(model.input, x2_layer.output)
    x2 = x2_model.predict([t1, t2])

    attention_weights = np.squeeze(a_layer.att(x2, x1, return_attention_scores=True)[1])
    t = t1

    fig, axes = plt.subplots(nrows=2, ncols=attention_weights.shape[0] // 2, sharex=True, sharey=True)
    for (matrix, ax) in zip(attention_weights, axes.flatten()):
        ax.imshow(matrix, origin="lower", cmap="cividis")
        ax.plot(np.squeeze(t) * 200 / (1.1 * np.max(t)), color="white", linewidth=0.5)
        # ax.plot(np.squeeze(t2) * 200, color="darkgrey", alpha=0.5, linestyle="dotted")
    fig.tight_layout()

    plt.savefig(fname, dpi=300)
