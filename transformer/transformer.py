#!/usr/bin/env pygpu

import numpy as np
import tensorflow as tf
from tensorflow.keras import layers, models
from tools import get_data, plot_example_traces, positional_encoding, plot_training, plot_confusion, plot_attention_scores
import matplotlib.pyplot as plt

tracelength = 200
number_samples = 5000
network_dims = 32
num_heads = 8
ff_dim = 32
n_classes = 4
N_blocks = 1
dropout_rate = 0.1
n_epochs = 15

traces_1, traces_2, labels = get_data(number_samples, tracelength, n_classes)

# The dataset contains two types of time traces with several peaks. While trace_1 and trace_2 always share n_classes-1 peaks,
# trace_1 has an additional peak at a random timestep. The network should count the number of peaks that occured in both traces
# before the individual peak in trace_1 and classify the traces accordingly in one of n_classes classes (from no peak before the
# individual peak to all n_classes-1 peaks before the individual peak).

# Example Plots
plot_example_traces(traces_1[1234], traces_2[1234], labels[1234], "trace_example_1.png", n_classes)
plot_example_traces(traces_1[4321], traces_2[4321], labels[4321], "trace_example_2.png", n_classes)


# Define TransformerBlock based on https://arxiv.org/pdf/1706.03762.pdf
class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=dropout_rate):
        super(TransformerBlock, self).__init__()
        self.att = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim // num_heads)
        self.ffn = models.Sequential([layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim)])
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)

    def call(self, input1, input2, training=None):
        attn_output = self.att(input1, input2)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(input1 + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)


# Transformer network:
# Define inputs
inp_1 = layers.Input((tracelength, 1))
inp_2 = layers.Input((tracelength, 1))

# Embedding
embedding = layers.Dense(network_dims, activation="tanh")

# Positional encoding
pos_encs = positional_encoding(tracelength, network_dims)

# Define inputs
inp_1_em = embedding(inp_1)
inp_2_em = embedding(inp_2)

x1 = layers.Add()([inp_1_em, pos_encs])
x2 = layers.Add()([inp_2_em, pos_encs])

# Self attention:
for i in range(N_blocks):
    x1 = TransformerBlock(embed_dim=network_dims, num_heads=num_heads, ff_dim=ff_dim)(x1, x1)
    x2 = TransformerBlock(embed_dim=network_dims, num_heads=num_heads, ff_dim=ff_dim)(x2, x2)

# Cross attention
x = TransformerBlock(embed_dim=network_dims, num_heads=num_heads, ff_dim=ff_dim)(x2, x1)

# Combine
x = layers.GlobalAveragePooling1D()(x)

# Get final prediction
x = layers.Dense(n_classes)(x)
out = layers.Softmax()(x)

# Construct network
model = tf.keras.models.Model([inp_1, inp_2], out)
optimizer = tf.keras.optimizers.Adam(learning_rate=5e-3)
model.compile(optimizer, metrics=["acc"], loss="CategoricalCrossentropy")
model.summary()

# Training
history = model.fit([traces_1, traces_2], labels, batch_size=32, epochs=n_epochs, validation_split=0.1, verbose=2)
plot_training(history)

# Test model
test_1, test_2, test_labels = get_data(1000, tracelength, n_classes)
prediction = np.argmax(model.predict([test_1[..., np.newaxis], test_2[..., np.newaxis]]), axis=-1)
truth = np.argmax(test_labels, axis=-1)
plot_confusion(prediction, truth)

# Plot attention - This has to be adjusted, if layers are changed
attention_layer = -4  # Index of TransformerBlock of which the attention should be plotted

plot_example_traces(test_1[123], test_2[123], test_labels[123], "test_trace_example_1.png", n_classes)
plot_attention_scores(model, attention_layer, test_1[123][np.newaxis, :, np.newaxis], test_2[123][np.newaxis, :, np.newaxis], "test_trace1_attention_example_1.png")

plot_example_traces(test_1[321], test_2[321], test_labels[321], "test_trace_example_2.png", n_classes)
plot_attention_scores(model, attention_layer, test_1[321][np.newaxis, :, np.newaxis], test_2[321][np.newaxis, :, np.newaxis], "test_trace1_attention_example_2.png")
