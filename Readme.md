# Vispa-Examples

This Repo is used to manage the example pages on the Vispa-Cluster

## Deployment

The repo cannot be deployed automatically with the gitlab CI because we don't user a Kubernetes cluster.
Therefore, the newest changes need to be pulled, i.e.:

```
cd /home/public/vispa_examples

git pull
```

The `setup_page.sh` is used as a githook, to copy the `main.html` to the right directory (`/home/public/CERN/main.html`, whyever
this is the place...).

To update the post-merge hook, simply edit the script and add it to the posthooks *inside the production repository*:

```
cp setup_page.sh .git/hooks/post-merge

chmod +x .git/hooks/post-merge
```
