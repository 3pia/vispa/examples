import numpy as np
import pylab

# read in public auger data
ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.genfromtxt("/home/public/auger/examples/data/auger_public_2014_04_17.txt", unpack=True)
log_energies = np.log10(energies * 1.e18)  # calculate logarithm of energy

# create a figure with a larger-than default size as it will hold four subplots
fig = pylab.figure(figsize=[12.4, 8.2])

# create a histogram containing the cosmic ray energies
ax1 = fig.add_subplot(221)
ax1.hist(log_energies, bins=40, range=(16.5, 20), histtype='stepfilled')
ax1.set_xlabel('log10(Energy [eV])')
ax1.set_ylabel('Events')

# naive 'unprojected' map of data
ax3 = fig.add_subplot(222)
ax3.scatter(gal_lons, gal_lats, c=log_energies, linewidth=0)
ax3.set_xlabel('Longitude [rad]')
ax3.set_ylabel('Latitude [rad]')

# convert degrees to radian
l = np.deg2rad(gal_lons)
b = np.deg2rad(gal_lats)

# Hammer projected map of data
ax4 = fig.add_subplot(223, projection='hammer')
events = ax4.scatter(l, b, c=log_energies, linewidth=0)
ax4.set_ylabel("Latitude [deg]")  # set y-axis label, x-axis label will not be set as it will not be visible

fig.tight_layout()  # improve plot layout

# plot color bar
cbar4 = pylab.colorbar(events, orientation='horizontal', pad=0.05, aspect=40)
cbar4.set_label("$\log_{10}$(Energy/[eV])")
cbar4.set_ticks(np.arange(17.0, 20.5, 0.5))

# Hammer projected map of events with highest energy
selection = energies > 19  # select only cosmic rays with energy > 10EeV
ax1 = fig.add_subplot(224, projection='hammer')
events2 = ax1.scatter(l[selection], b[selection], c=log_energies[selection], linewidth=0)
ax1.set_ylabel("Latitude [deg]")  # set y-axis label, x-axis label will not be set as it will not be visible


# plot color bar
cbar1 = pylab.colorbar(events2, orientation='horizontal', pad=0.05, aspect=40)
cbar1.set_label("$\log_{10}$(Energy/[eV])")
cbar1.set_ticks(np.arange(19.0, 20.5, 0.1))


fig.savefig('auger_skymap.png')  # save plot
