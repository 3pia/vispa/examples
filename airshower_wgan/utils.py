import numpy as np
from tensorflow import keras
import tensorflow as tf
import tensorflow.keras.backend as K
import matplotlib.pyplot as plt

layers = keras.layers


def ReadInData():
    '''Reads in the trainings data'''
    filenames = "/home/public/deeplearning/data/Data.npz"
    data = np.load(filenames)
    return data['shower_maps'], data['Energy']

# plot real signal patterns
def rectangular_array(n=9):
    """ Return x,y coordinates for rectangular array with n^2 stations. """
    n0 = (n - 1) / 2
    return (np.mgrid[0:n, 0:n].astype(float) - n0)


def plot_footprints(footprints):
    fig, axes = plt.subplots(2,2)
    axes = axes.squeeze()
    axes = axes.flatten()

    for i,j in enumerate(np.random.choice(footprints.shape[0], 4)):
        ax = axes[i]
        footprint=footprints[j,...,0]
        xd, yd = rectangular_array()
        mask = footprint != 0
        mask[5, 5] = True
        marker_size = 50 * footprint[mask]
        plot = ax.scatter(xd, yd, c='grey', s=10, alpha=0.3, label="silent")
        circles = ax.scatter(xd[mask], yd[mask], c=footprint[mask],
                               s=marker_size, alpha=1, label="loud")
        cbar = fig.colorbar(circles, ax=ax)
        cbar.set_label('signal [a.u.]')
        ax.grid(True)

    fig.tight_layout()
    return fig

def generator_model(latent_size):
    """ Generator network """
    latent = layers.Input(shape=(latent_size,), name="noise")
    z = layers.Dense(latent_size)(latent)
    z = layers.Reshape((1, 1, latent_size))(z)
    z = layers.UpSampling2D(size=(3, 3))(z)
    z = layers.Conv2D(256, (3, 3), padding='same',
                      kernel_initializer='he_normal', activation='relu')(z)
    z = layers.Conv2D(256, (3, 3), padding='same',
                      kernel_initializer='he_normal', activation='relu')(z)
    z = layers.UpSampling2D(size=(3, 3))(z)
    z = layers.Conv2D(128, (3, 3), padding='same',
                      kernel_initializer='he_normal', activation='relu')(z)
    z = layers.Conv2D(128, (3, 3), padding='same',
                      kernel_initializer='he_normal', activation='relu')(z)
    z = layers.Conv2D(1, (3, 3), padding='same', kernel_initializer='he_normal')(z)
    z = layers.Activation("relu")(z)
    return keras.models.Model(latent, z, name="generator")


# build critic
# Feel free to modify the critic model
def critic_model():
    image = layers.Input(shape=(9, 9, 1), name="images")
    x = layers.Conv2D(64, (3, 3), padding='same', kernel_initializer='he_normal',
                      input_shape=(9, 9, 1))(image)
    x = layers.LeakyReLU()(x)
    x = layers.Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal')(x)
    x = layers.LeakyReLU()(x)
    x = layers.Conv2D(128, (3, 3), padding='same', strides=(2, 2),
                      kernel_initializer='he_normal')(x)
    x = layers.LeakyReLU()(x)
    x = layers.Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal')(x)
    x = layers.LeakyReLU()(x)
    x = layers.Conv2D(256, (3, 3), padding='same', strides=(2, 2),
                      kernel_initializer='he_normal')(x)
    x = layers.LeakyReLU()(x)
    x = layers.Flatten()(x)
    x = layers.Dense(128)(x)
    x = layers.LeakyReLU()(x)
    x = layers.Dense(1)(x)  # no activation!
    return keras.models.Model(image, x, name="critic")


def make_trainable(model, trainable):
    ''' Freezes/unfreezes the weights in the given model '''
    for layer in model.layers:
        # print(type(layer))
        if type(layer) is layers.BatchNormalization:
            layer.trainable = True
        else:
            layer.trainable = trainable
            
class UniformLineSampler(tf.keras.layers.Layer):
    def __init__(self, batch_size):
        super().__init__()
        self.batch_size = batch_size

    def call(self, inputs, **kwargs):
        weights = K.random_uniform((self.batch_size, 1, 1, 1))
        return(weights * inputs[0]) + ((1 - weights) * inputs[1])

    def compute_output_shape(self, input_shape):
        return input_shape[0]
