#!/usr/bin/env pygpu
import torch
import numpy as np
import train_func as t
import model_func as m
import plotting_func as p

# Example for simple function to be learned by a conditional Invertible Neural Network

device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Training & network parameters
cINN_kw = {'dim': 2,        # dimension of input x & output z (latents)
           'dim_c': 100,         # dimension of condition (observables y)
           'N_blocks': 6,       # number of multiplicative layer
           'coupling_layer_architecture': 'RNVP',  # architecture type of coupling layer, e.g. GLOW or RNVP
           'use_permutation': True,  # if a permutation layer after every multiplicative layer is used
           'exponent_clamping': 1.9,  # "soft clamping for the multiplicative component"
           'init_scale': 0.1,  # initialize model parameters from a normal distribution with this sigma
           'verbose_construction': False  # if the network structure should be printed or not
           }

training_kw = {'lr_init': 1.0e-3,     # initial learning rate
               'batch_size': 500,   # batch size (number of data sets used per epoch)
               'n_epochs': 50,      # total number of epochs for training
               'n_its_per_epoch': 100,   # number of iterations per epoch
               'pre_low_lr': 0,  # use int for number of iterations with surpressed learning rate (*1e-1) before actual training epochs start
               'adam_betas': (0.9, 0.999),  # Adam optimizer beta parameters
               'cosine_scheduler': True,  # cosine scheduler for the learning rate, starting at lr_init & falling to final_lr
               'final_lr': 1e-5,  # final learning rate
               'weight_decay': 1e-5   # L2 weight regularization of model parameters
               }

# Create example training & test data
# Here:
# Observables y: 100 Gaussian distributed values with standard deviation s and mean m
# -> dim_c has to be 100
# System parameters x: standard deviation s and mean m
# -> dim has to be 2
# The network will see the 100 Gaussian distributed values and has to predict the mean & standard deviation of
# the underlying distribution. Also, we will be able to give an estimate on the uncertainty of the network's
# prediction by looking at the whole posterior distribution. This would also unveil correlations between the parameters.


def generate(N, seed):
    """
    Generates N training data sets with numpy and converts them to torch tensors
    """
    np.random.seed(seed)
    s = np.random.uniform(0.1, 2.0, N)  # range of standard deviations that the network will see
    m = np.random.uniform(-5, 5, N)  # range of means that the network will see
    x = np.vstack((s, m))
    y = np.random.normal(loc=m, scale=s, size=(100, N))
    return torch.tensor(x, device=device).float().T, torch.tensor(y, device=device).float().T  # shapes: x: (N, 2), y: (N, 100)


x_test, y_test = generate(training_kw['batch_size']*training_kw['n_its_per_epoch'], seed=1)
x_train, y_train = generate(training_kw['batch_size']*training_kw['n_its_per_epoch']*10, seed=2)

# Use DataLoader Object from Pytorch
test_loader = torch.utils.data.DataLoader(
    torch.utils.data.TensorDataset(x_test, y_test),
    batch_size=training_kw['batch_size'], shuffle=False, drop_last=True)

train_loader = torch.utils.data.DataLoader(
    torch.utils.data.TensorDataset(x_train, y_train),
    batch_size=training_kw['batch_size'], shuffle=True, drop_last=True)

# Start training
filename_out = 'test_model_gaussian.pt'

t.train(cINN_kw, training_kw, train_loader, test_loader, filename_out)

# Start evaluation

# Load model and training, set to evaluation mode
model_class = m.Model(cINN_kw, training_kw)
model = model_class.model
state_dicts = torch.load(filename_out, map_location=device)
model.load_state_dict(state_dicts['net'])
model.eval()

# Plot loss
p.plot_loss()

# Create posteriors & plot them
posterior_dict = p.plot_posteriors(x_test, y_test, 0, model, cINN_kw)

# Plot predicted observables with mean posterior values for free model parameters (sigma & mean of Gaussian)
p.plot_observables(posterior_dict, x_test.cpu().numpy(), y_test.cpu().numpy(), 0)
