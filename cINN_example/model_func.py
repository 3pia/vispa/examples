import FrEIA.framework as Ff
import FrEIA.modules as Fm
import torch
import torch.nn as nn


# Define subnetworks for s & t functions in reversible blocks
class F_fully_connected(nn.Module):
    '''Fully connected tranformation, not reversible (doesn't have to be)'''

    def __init__(self, size_in, size, internal_size=128, dropout=0.0):
        super().__init__()
        if not internal_size:
            internal_size = 2*size
        self.d1 = nn.Dropout(p=dropout)
        self.d2 = nn.Dropout(p=dropout)
        self.d2b = nn.Dropout(p=dropout)
        self.fc1 = nn.Linear(size_in, internal_size)
        self.fc2 = nn.Linear(internal_size, internal_size)
        self.fc2b = nn.Linear(internal_size, internal_size)
        self.fc3 = nn.Linear(internal_size, size)
        self.nl1 = nn.ReLU()
        self.nl2 = nn.ReLU()
        self.nl2b = nn.ReLU()

    def forward(self, x):
        out = self.nl1(self.d1(self.fc1(x)))
        out = self.nl2(self.d2(self.fc2(out)))
        out = self.nl2b(self.d2b(self.fc2b(out)))
        out = self.fc3(out)
        return out


class Model:
    """
    Class to set up cINN model for training.
    """

    def __init__(self, cINN_kw, training_kw, **kwargs):
        """
        :param cINN_kw: dictionary containing information on the INN parameters
                       (e.g. #invertible blocks with 'N_blocks')
        :param training_kw: dictionary containing information on the training
                            setup (e.g. initial learning rate 'lr_init')
        """
        super().__init__()
        self.cINN_kw = cINN_kw
        self.training_kw = training_kw
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self._init_model()
        self._init_optim()

    def _init_model(self):
        cond = Ff.ConditionNode(self.cINN_kw['dim_c'], name='condition')  # dimension of observables y
        nodes = [Ff.InputNode(self.cINN_kw['dim'], name='input')]  # dimension of system parameters x
        for k in range(self.cINN_kw['N_blocks']):
            if self.cINN_kw['coupling_layer_architecture'] == 'RNVP':
                nodes.append(Ff.Node([nodes[-1].out0], Fm.RNVPCouplingBlock,
                                     {'subnet_constructor': F_fully_connected,
                                      # 'F_args': {'internal_size': self.cINN_kw['internal_size']},
                                      'clamp': self.cINN_kw['exponent_clamping'],
                                      }, conditions=[cond],
                                     name='coupling_{}'.format(k)))
            elif self.cINN_kw['coupling_layer_architecture'] == 'GLOW':
                nodes.append(Ff.Node([nodes[-1].out0], Fm.glow_coupling_layer,
                                     {'F_class': F_fully_connected,
                                      'F_args': {'internal_size': self.cINN_kw['internal_size']},
                                      'clamp': self.cINN_kw['exponent_clamping'],
                                      }, conditions=[cond],
                                     name='coupling_{}'.format(k)))
            else:
                KeyError('Choose valid coupling layer architecture.')
            if self.cINN_kw['use_permutation']:
                nodes.append(Ff.Node([nodes[-1].out0], Fm.PermuteRandom, {'seed': k},
                                     name='permute_{}'.format(k)))

        nodes.append(Ff.OutputNode(nodes[-1], name='output'))
        self.model = Ff.GraphINN(nodes + [cond], verbose=self.cINN_kw['verbose_construction'])
        self.model.to(self.device)

    def _init_optim(self):
        params_trainable = list(filter(lambda p: p.requires_grad, self.model.parameters()))
        for p in params_trainable:
            p.data = self.cINN_kw['init_scale'] * torch.randn(p.data.shape).to(self.device)

        self.optim = torch.optim.Adam(params_trainable, lr=self.training_kw['lr_init'],
                                      betas=self.training_kw['adam_betas'],
                                      eps=1e-6, weight_decay=self.training_kw['weight_decay'])

    def model(self):
        return self.model

    def optim_step(self):
        self.optim.step()
        self.optim.zero_grad()

    def save(self, name):
        torch.save({'opt': self.optim.state_dict(),
                    'net': self.model.state_dict()}, name)

    def load(self, name):
        state_dicts = torch.load(name, map_location=self.device)
        self.model.load_state_dict(state_dicts['net'])
        try:
            self.optim.load_state_dict(state_dicts['opt'])
        except ValueError:
            print('Cannot load optimizer for some reason or other')
