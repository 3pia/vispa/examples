import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.compat.v1.keras.layers import *
from tensorflow.compat.v1.keras.layers import LeakyReLU
from tensorflow.compat.v1.keras.regularizers import l1_l2
from tensorflow.compat.v1.keras.models import Sequential


def get_session(gpu_fraction=0.40):
    ''' Allocate only a fraction of the GPU RAM - (1080 GTX 8Gb)'''
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
    return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


def plot_images(images, figsize=(10, 10), fname=None):
    """ Plot some images """
    n_examples = len(images)
    dim = np.ceil(np.sqrt(n_examples))
    plt.figure(figsize=figsize)
    for i in range(n_examples):
        plt.subplot(dim, dim, i + 1)
        img = np.squeeze(images[i])
        plt.imshow(img, cmap=plt.cm.Greys)
        plt.axis('off')
    plt.tight_layout()
    if fname is not None:
        plt.savefig(fname)
    plt.close()


def make_trainable(model, trainable):
    """ Helper to freeze / unfreeze a model """
    model.trainable = trainable
    for l in model.layers:
        l.trainable = trainable


def generator_model(inp):
    """ Generator network """
    z = Dense(7 * 7 * 128)(inp)
    z = BatchNormalization(scale='False')(z)
    z = Activation('relu')(z)
    z = Reshape([7, 7, 128])(z)
    z = UpSampling2D(size=(2, 2))(z)
    z = Conv2D(128, (5, 5), padding='same')(z)
    z = BatchNormalization(scale='False')(z)
    z = Activation('relu')(z)
    z = UpSampling2D(size=(2, 2))(z)
    z = Conv2D(64, (5, 5), padding='same')(z)
    z = BatchNormalization(scale='False')(z)
    z = Activation('relu')(z)
    return Conv2D(1, (5, 5), padding='same', activation='sigmoid')(z)


def discriminator_model(inp, drop_rate=0.25):
    """ Discriminator network """
    z = Conv2D(32, (5, 5), padding='same', strides=(2, 2), activation='relu')(inp)
    z = LeakyReLU(0.2)(z)
    z = Dropout(drop_rate)(z)
    z = Conv2D(64, (5, 5), padding='same', strides=(2, 2), activation='relu')(z)
    z = LeakyReLU(0.2)(z)
    z = Dropout(drop_rate)(z)
    z = Conv2D(128, (5, 5), padding='same', strides=(2, 2), activation='relu')(z)
    z = LeakyReLU(0.2)(z)
    z = Dropout(drop_rate)(z)
    z = Flatten()(z)
    z = Dense(256, activity_regularizer=l1_l2(1e-5))(z)
    z = LeakyReLU(0.2)(z)
    z = Dropout(drop_rate)(z)
    return Dense(2, activation='softmax')(z)
