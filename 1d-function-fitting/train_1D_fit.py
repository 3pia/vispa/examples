#!/usr/bin/env -S submit -M 1900 -m 7500 -f python

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt


# -----------------------------------------------------------
# Generate Data
# -----------------------------------------------------------
def some_complicated_function(x):
    return (
        (np.abs(x)) ** 0.5
        + 0.1 * x
        + 0.01 * x ** 2
        + 1
        - np.sin(x)
        + 0.5 * np.exp(x / 10.0)
    ) / (0.5 + np.abs(np.cos(x)))


N = 10 ** 4  # number of training samples
# Note: "[:,None]" reshapes to (N,1) as required by our DNN
xdata = np.linspace(-10, 10, N)[:, None]
ydata = some_complicated_function(xdata)


# -----------------------------------------------------------
# Settings
# -----------------------------------------------------------
nb_layers = 3  # number of hidden layers
nb_nodes = 20  # number of nodes per layer
activation = "relu"  # activation function
epochs = 400  # number of training epochs
save_period = 80  # number of epochs between epochs of network

# (additional) input features to train on
xtrain = xdata
# xtrain = np.c_[ xdata, xdata**2, np.sin(xdata), np.cos(xdata)]  # some extra features


# -----------------------------------------------------------
# Define model
# -----------------------------------------------------------
model = tf.keras.models.Sequential(name="1Dfit")
model.add(tf.keras.layers.Dense(nb_nodes, activation=activation, input_dim=xtrain.shape[1]))
for i in range(nb_layers - 1):
    model.add(tf.keras.layers.Dense(nb_nodes, activation=activation))
model.add(tf.keras.layers.Dense(1))

print(model.summary())


# -----------------------------------------------------------
# Training
# -----------------------------------------------------------

model.compile(loss="mse", optimizer="adam")

results = model.fit(
    xtrain,
    ydata,
    batch_size=128,
    epochs=epochs,
    verbose=2,
    callbacks=[
        tf.keras.callbacks.ModelCheckpoint(
            "{epoch:02d}.weights.h5", save_weights_only=True, save_freq=save_period,
        ),
        tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name)),
    ],
)

# -----------------------------------------------------------
# Plot
# -----------------------------------------------------------
fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(12, 8))
plt.tight_layout()

# plot data and fit
ax1.plot(xdata, ydata, color="black", label="data")
epochs = range(save_period, epochs + 1, save_period)

colors = [plt.cm.jet((i + 1) / float(len(epochs) + 1)) for i in range(len(epochs))]

for i, epoch in enumerate(epochs):
    model.load_weights("{epoch:02d}.weights.h5".format(epoch=epoch))
    ypredict = model.predict(xtrain).T[0]
    ax1.plot(xdata, ypredict, color=colors[i], label=epoch)
    ax2.plot(epoch, results.history["loss"][epoch - 1], color=colors[i], marker="o")

ax1.set(
    xlabel="x",
    ylabel="some_complicated_function(x)",
    xlim=(-10, 13),
    title="%i layers, %i nodes, %s" % (nb_layers, nb_nodes, activation),
)
ax1.grid(True)
ax1.legend(loc="upper right", title="Epochs")

# plot history of loss
ax2.plot(results.history["loss"], color="black")
ax2.set(xlabel="epoch", ylabel="loss")
ax2.grid(True)
ax2.semilogy()

fig.savefig("fit_{}.png".format(model.name), bbox_inches="tight")
