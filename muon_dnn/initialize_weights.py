import numpy as np


def initialize_weights(layers):
    """ Initialize the weights from a normal distribution
    The number of layers is given as input.
    A dictionary with all the matrices W and b for all layers
    is returned
    """
    # Fix the seed, same results for everyone :)
    np.random.seed(3)
    weights = {}
    for i in range(1, len(layers)):
        weights['W' + str(i)] = np.random.randn(layers[i], layers[i-1])
        weights['b' + str(i)] = np.random.randn(layers[i], 1)
    return weights
