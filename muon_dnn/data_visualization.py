import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


train = pd.read_hdf('/net/scratch/deeplearning/muon_dnn/data/train.hdf')
val = pd.read_hdf('/net/scratch/deeplearning/muon_dnn/data/val.hdf')
test = pd.read_hdf('/net/scratch/deeplearning/muon_dnn/data/test.hdf')


for df, name in zip([train, val, test], ['train', 'val', 'test']):
    print('Making plots for the', name, 'set')

    # Four plots of the distribution of the four input variables
    fig, axls = plt.subplots(2, 2)

    # make axls a generator; returns its elements by calling next(axls)
    axls = (ax for ax in axls.flatten())

    ax = next(axls)
    bins = np.arange(18.5, 20.1, .1)
    ax.hist(df['logenergy'], bins, ec='C0')
    ax.set_xlabel('$\log_{10}(E_\mathrm{SD}/\mathrm{eV})$')
    ax.set_ylabel('Entries')
    ax.set_ylim(top=2200)

    ax = next(axls)
    bins = np.arange(1, 2.1, .1)
    ax.hist(df['sectheta'], bins, ec='C0')
    ax.set_xlabel(r'$\sec\,\theta$')
    ax.set_ylabel('Entries')
    ax.set_ylim(top=3900)

    ax = next(axls)
    bins = np.arange(0, 100, 2)
    ax.hist(df['total_signal'], bins, ec='C0')
    ax.set_ylabel('Entries')
    ax.set_xlabel('$S$ [VEM]')
    ax.set_yscale('log')
    ax.set_ylim(top=3000)

    ax = next(axls)
    bins = np.arange(0, 3000, 100)
    ax.hist(df['r'], bins, ec='C0')
    ax.set_xlabel('$r$ [m]')
    ax.set_ylabel('Entries')
    ax.set_ylim(top=1900)
    plt.tight_layout()
    fig.savefig(name + '_x.png')


    # Plot of the distribution of the target variable
    fig, ax = plt.subplots(1, 1)
    bins = np.arange(0, 70, 2)
    ax.hist(df['muon_signal_mc'], bins, color='C1', ec='C1');
    # ax.set_xlim(right=.5)
    ax.set_xlabel('$S^\mu$ [VEM]')
    ax.set_ylabel('Entries')
    ax.set_ylim(top=3900)
    fig.savefig(name + '_y.png')
