#!/usr/bin/env pygpu

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tensorflow import keras
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from sklearn.preprocessing import StandardScaler

# Parameters of our neural network
layers = [4, 8, 12, 1]

# Define the model
model = Sequential()
model.add(Dense(8))
model.add(Dense(12))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')

# Load the data
train = pd.read_hdf('/net/scratch/deeplearning/muon_dnn/data/train.hdf')
val = pd.read_hdf('/net/scratch/deeplearning/muon_dnn/data/val.hdf')
test = pd.read_hdf('/net/scratch/deeplearning/muon_dnn/data/test.hdf')

# Transform the data from pandas DataFrame to numpy using .values
train_x = train[['logenergy', 'sectheta', 'total_signal', 'r']].values
train_y = train['muon_signal_mc'].values
val_x = val[['logenergy', 'sectheta', 'total_signal', 'r']].values
val_y = val['muon_signal_mc'].values
test_x = test[['logenergy', 'sectheta', 'total_signal', 'r']].values
test_y = test['muon_signal_mc'].values

# Scale the data to have mean 0 and std. dev. 1
# Each feature is scaled independently
sc = StandardScaler()
train_x = sc.fit_transform(train_x)
val_x = sc.transform(val_x)
test_x = sc.transform(test_x)

# Training
history = model.fit(train_x, train_y, epochs=500, batch_size=20000, validation_data=(val_x, val_y))

# Plot of the loss as a function of the epoch and the
# distribution of the differences between the true and predicted values
loss_vals, val_vals = history.history['loss'], history.history['val_loss']
fig, axls = plt.subplots(1, 2)
ax = axls[0]
ax.plot(loss_vals[1:], label='Train')
ax.plot(val_vals[1:], '--', label='Validation')
ax.set_xlabel('Epoch')
ax.set_ylabel('Mean Squared Error')
ax.legend()
ax = axls[1]
y_pred_test = model.predict(test_x)[:, 0]
diff = y_pred_test - test_y
mean = diff.mean()
std = diff.std()
ax.hist(diff, bins=np.arange(-5, 5.01, .1))
ax.set_ylabel('Entries')
ax.set_xlabel('$\hat{y}-y$ [VEM]')
ax.text(0, 50, 'mean={:.2f}'.format(mean) + '\n' + 'std={:.2f}'.format(std))
plt.tight_layout()
fig.savefig('./nn-loss')
