from activation_function import relu_prime

def compute_gradient(layers, weights, cache, y_true, inp):
    """ Compute the gradient of all the weights in all layers"""

    grad = {}
    # Baseline term, will be updated at each layer
    base = 2 / len(y_true) * (cache['a' + str(len(layers)-1)]-y_true.reshape((1, -1))) * relu_prime(cache['z' + str(len(layers)-1)])

    # Remember a0 is the input
    cache['a0'] = inp.T

    for i in range(len(layers)-1, 0, -1):
        grad['b' + str(i)] = base.sum(axis=1).reshape((-1, 1))
        grad['W' + str(i)] = base.dot(cache['a' + str(i-1)].T)

        # If we are at i == 1 we are in the last layer and we are done
        if i != 1:
            base = weights['W' + str(i)].T.dot(base) * relu_prime(cache['z' + str(i-1)])
    return grad

def backward(layers, weights, cache, y_true, inp, lr=.001):
    """ Compute the gradient, update all the parameters
    with the learning rate lr and return the new values"""
    grad = compute_gradient(layers, weights, cache, y_true, inp)
    new_weights = {}
    for key, val in weights.items():
        new_weights[key] = weights[key] - lr * grad[key] 
    return new_weights
