import numpy as np
from forward import forward


def loss(y_true, y_pred):
    return ((y_true-y_pred)**2).sum() / len(y_true)


# This function prints the gradients obtained with backprop and finite differences
def numeric_gradient(layers, weights, y_true, inp):
    """ Compute the numeric gradient using
    f'(x, a, ...) = (f(x, a+eps,...)-f(x, a-eps,...)) / 2eps
    """
    eps = 1e-7
    dic = {}
    for key, val in weights.items():
        # Grad set to zero by default
        dic[key] = np.zeros(val.shape)
        for i in range(val.shape[0]):
            for j in range(val.shape[1]):
                # Compute loss(x, a+eps,...)
                val[i, j] += eps
                a, _ = forward(layers, weights, inp)
                c1 = loss(y_true, a)
                # Compute loss(x, a-eps,...)
                val[i, j] -= 2 * eps
                a, _ = forward(layers, weights, inp)
                c2 = loss(y_true, a)
                # Return val[i, j] to its original value
                val[i, j] += eps
                # Numerical gradient for one weight
                dic[key][i, j] = (c1 - c2) / (2 * eps)
    return dic
