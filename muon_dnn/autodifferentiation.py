import tensorflow as tf
import numpy as np

x = tf.Variable(2.0)
with tf.GradientTape() as tape:
    y = x**2
grad_square = tape.gradient(y, x).numpy()
with tf.GradientTape() as tape:
    z = tf.tanh(x**(x / tf.exp(x)))
grad_complicated = tape.gradient(z, x).numpy()
print('Gradient of x^2 at 2 is', grad_square)
print('Gradient of the complicated function at 2 is', grad_complicated)

