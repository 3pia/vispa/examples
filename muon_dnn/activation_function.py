import numpy as np

def relu(x):
    """ Rectified Linear Unit (ReLU) activation function
    f(x) = max(x, 0)
    """
    return np.maximum(x, 0)

def relu_prime(x):
    """ Derivative of ReLU
    f'(x) = 1 for x > 0
    f'(x) = 0 for x < 0  
    """
    # Make a copy to avoid writing on the original input
    y = np.array(x, copy=True) 
    y[y <= 0] = 0
    y[y > 0] = 1
    return y


# If run as main, produce a plot of ReLU and its derivative
if __name__ == '__main__':
    import matplotlib.pyplot as plt

    # Create plot
    fig, ax = plt.subplots(1, 1)
    ax.set_xlabel('x')
    ax.set_ylabel('$f(x)$')
    
    # Make 100 points evenly spaced between -10 and 10
    x = np.linspace(-10, 10, 100)

    # ReLU plot
    y = relu(x)
    ax.plot(x, y, label='$f(x)=\mathrm{ReLU}(x)$')

    # Derivative plot
    y = relu_prime(x)
    ax.plot(x, y, label='$f(x)=\mathrm{ReLU}\'(x)$')

    ax.legend()
    fig.savefig('activation_function.png')
