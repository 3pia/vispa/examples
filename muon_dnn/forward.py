from activation_function import relu


def forward(layers, weights, inp):
    """Implement the forward step
    Given the number of layers, a dictionary with the
    weights and the input, return the output of the neural network and a
    dictionary with the values of a and z"""

    # Transpose the input to reshape it as (features, samples)
    prev_inp = inp.T

    cache = {}
    for i in range(1, len(layers)):
        # The linear transformation, .dot is the dot product in numpy
        z = weights['W' + str(i)].dot(prev_inp)+weights['b' + str(i)]

        a = relu(z)
        cache['z' + str(i)] = z
        cache['a' + str(i)] = a

        # Prepare the input for the next layer
        prev_inp = a

    return a, cache
