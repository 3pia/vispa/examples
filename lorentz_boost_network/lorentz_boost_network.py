#!/usr/bin/env pygpu

import numpy as np
import tensorflow as tf
from lbn import LBN, LBNLayer


# load the data
data_directory = "/net/scratch/cms/data/public/ttHbb/"

x_train, y_train = np.load(data_directory + "x_train.npy"), np.load(data_directory + "y_train.npy")
x_test, y_test = np.load(data_directory + "x_test.npy"), np.load(data_directory + "y_test.npy")

# build the model
model = tf.keras.models.Sequential()

# init lbn layer
lbn_layer = LBNLayer(10, boost_mode=LBN.PAIRS)

# add the LBN layer
model.add(lbn_layer)

# add dense layers
model.add(tf.keras.layers.Dense(256))
model.add(tf.keras.layers.Dense(256))
model.add(tf.keras.layers.Dense(256))
model.add(tf.keras.layers.Dense(2))

# compile the model
model.compile(
    optimizer="adam",
    loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
    metrics=["accuracy"],
)

# fit the model to the data
model.fit(x_train, y_train, epochs=5, batch_size=256, validation_data=(x_test, y_test), verbose=2)
