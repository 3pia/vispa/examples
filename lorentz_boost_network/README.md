# Lorentz-Boost Network

Example of the Tensorflow implementation of the [Lorentz-Boost Network](https://github.com/riga/LBN) from [arXiv:1812.09722](https://arxiv.org/abs/1812.09722).
