#!/bin/sh
# ths file should be configured as a local
# post-merge hook on the cluster, so the
# main.html is always in the right spot

echo "Copying main.html to target directory"
cp main.html /home/public/CERN/main.html
#echo "Linking File"
#if [ -e "/home/public/CERN/main.html" ]; then
	#echo "main.html is already linked, skipping"
#else
	#echo "Creating symlink"
	#ln -s main.html /home/public/CERN/main.html
#fi
