#!/usr/bin/env -S submit -M 3900 -m 7500 -f python
# example from www.deeplearningphysics.org/

from matplotlib import pyplot as plt
import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

layers = keras.layers
print("keras", keras.__version__)

# ############################
# Load and prepare dataset
# ############################
# In this task, we use a simulation of cosmic-ray-induced air showers that are measured by radion antennas.
# For more information, see https://arxiv.org/abs/1901.04079.
# The task is to design an RNN that is able to identify if the measured signal traces (shortened to 500 time steps) contains a signal or not.

f = np.load("/net/scratch/deeplearning/radio_signal_data/radio_data.npz")
n_train = 40000

x_train, x_test = f["traces"][:n_train], f["traces"][n_train:]  # measured traces (signal + colored noise)
signals = f["signals"]  # signal part (only available for cosmic-ray events)

labels = (signals.std(axis=-1) != 0).astype(float)  # define training label (1=cosmic event, 0=noise)
y_train, y_test = labels[:n_train], labels[n_train:]

# Plot example signal traces
# Left: signal trace containing a cosmic-ray event. The underlying cosmic-ray signal is shown in red, the backgrounds + signal is shown in blue. Right: background noise.
fs = 180e6  # Sampling frequency of antenna setup 180 MHz
t = np.arange(500) / fs * 1e6
idx = np.random.randint(0, labels.sum()-1)
idx2 = np.random.randint(0, n_train - labels.sum())

plt.figure(1, (12, 4))
plt.subplot(1, 2, 1)
plt.plot(t, np.real(f["traces"][labels.astype(bool)][idx]), linewidth=1.5, color="b", label="Measured trace")
plt.plot(t, np.real(signals[labels.astype(bool)][idx]), linewidth=1.5, color="r", label="CR signal", alpha=0.6)
plt.ylabel('Amplitude / mV')
plt.xlabel('Time / $\mu \mathrm{s}$')
plt.legend()
plt.title("Cosmic-ray event")
plt.subplot(1, 2, 2)

plt.plot(t, np.real(x_train[~y_train.astype(bool)][idx2]), linewidth=1.5, color="b", label="Measured trace")
plt.ylabel('Amplitude / mV')
plt.xlabel('Time / $\mu \mathrm{s}$')
plt.legend()
plt.title("Noise event")

plt.grid(True)
plt.tight_layout()
plt.savefig("./example_trace.png")

# ############################
# Pre-processing of data and RNN training
# ############################

sigma = x_train.std()
x_train /= sigma
x_test /= sigma


# Start implementing and training your model HERE

model = keras.models.Sequential()
model.add(layers.Bidirectional(layers.LSTM(32, return_sequences=True), input_shape=(500,1)))
model.add(layers.Flatten())
model.add(layers.Dense(1, activation="sigmoid"))

model.summary()

model.compile(
    loss='binary_crossentropy',
    optimizer=keras.optimizers.Adam(1e-3, decay=0.00008),
    metrics=['accuracy'])

results = model.fit(x_train[...,np.newaxis], y_train,
                    batch_size=128,
                    epochs=10,
                    verbose=1,
                    validation_split=0.1,
                    callbacks = [keras.callbacks.ReduceLROnPlateau(factor=0.5, patience=5, verbose=1, min_lr=1e-5),
                                 keras.callbacks.EarlyStopping(patience=15, verbose=1)]
                    )

model.evaluate(x_test[..., np.newaxis], y_test)


# Plot loss and accuracy
plt.close("all")
plt.figure(1, (12, 4))
plt.subplot(1, 2, 1)
plt.plot(results.history['loss'])
plt.plot(results.history['val_loss'])
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper right')

plt.subplot(1, 2, 2)
plt.plot(results.history['accuracy'])
plt.plot(results.history['val_accuracy'])
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.tight_layout()
plt.savefig("./training_history.png")

