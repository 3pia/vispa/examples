#!/usr/bin/env pygpu

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tabulate import tabulate

# ---------------------------------------------------------
# Load and preprocess toy data
# ---------------------------------------------------------
data = np.load("/net/scratch/deeplearning/airshower/data.npz")

muons = data["T"]  # arrays of positions (x, y) and times t
mass = data["A"]  # mass number A
species = data["species"]  # simulated mass numbers (1, 4, 14, 56)


def muonImage(muons, xbins, tbins=None):
    """ Create histogram of muon hits for a single event """
    x, y, t = muons
    if tbins is None:
        # 2D histogram of spatial distribution
        return np.histogram2d(x, y, bins=(xbins, xbins))[0]
    else:
        # 3D histogram of spatial + time distribution
        return np.histogramdd((x, y, t), bins=(xbins, xbins, tbins))[0]


# Create 10x10 pixel image of total number of muon hits, no time dimension
npix = 10
xbins = np.linspace(-5000, 5000, npix + 1)  # pixel borders in [m]
image = np.array([muonImage(m, xbins) for m in muons])

# normalize images to hide total number of muons
image /= np.sum(image, axis=(1, 2))[:, None, None]

# reshape for neural network (add axis of size 1)
image = image.reshape(-1, npix, npix, 1)
mass_onehot = np.array([mass == a for a in species], dtype=float).T

# split train, validation and test samples
i0, i1, i2, i3 = 0, 30000, 34000, 40000
X_train, Y_train = image[i0:i1], mass_onehot[i0:i1]
X_valid, Y_valid = image[i1:i2], mass_onehot[i1:i2]
X_test, Y_test = image[i2:i3], mass_onehot[i2:i3]


# ----------------------------------------------------------
# Define model
# ----------------------------------------------------------
model = tf.keras.models.Sequential(
    [
        tf.keras.layers.Convolution2D(32, (3, 3), padding="valid", activation="relu", input_shape=(npix, npix, 1)),
        tf.keras.layers.Convolution2D(32, (3, 3), padding="valid", activation="relu"),
        tf.keras.layers.Convolution2D(64, (3, 3), padding="valid", activation="relu"),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(128, activation="relu"),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(4, activation="softmax"),
    ],
    name="airshower_mass",
)

print(model.summary())


# ----------------------------------------------------------
# Training
# ----------------------------------------------------------
model.compile(
    loss="categorical_crossentropy",
    optimizer=tf.keras.optimizers.Adam(lr=1e-3),
    metrics=["accuracy"],
)

batch_size = 32
result = model.fit(
    X_train,
    Y_train,
    batch_size=batch_size,
    epochs=11,
    verbose=1,
    validation_data=(X_valid, Y_valid),
    callbacks=[tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name))],
)

model.save("model_{}.h5".format(model.name))


# ----------------------------------------------------------
# Evaluation and Plots
# ----------------------------------------------------------
# evaluate performance
print("Model performance :")
headers = ["", "Loss", "Accuracy"]

table = [
    ["Train", *model.evaluate(X_train, Y_train, verbose=0, batch_size=128)],
    ["Validation", *model.evaluate(X_valid, Y_valid, verbose=0, batch_size=128)],
    ["Test", *model.evaluate(X_test, Y_test, verbose=0, batch_size=128)],
]

print(tabulate(table, headers=headers, tablefmt="orgtbl"))


# training curves
history = np.genfromtxt("history_{}.csv".format(model.name), delimiter=",", names=True)

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["loss"], label="training")
ax.plot(history["epoch"], history["val_loss"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="loss")
fig.savefig("loss_{}.png".format(model.name))

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["accuracy"], label="training")
ax.plot(history["epoch"], history["val_accuracy"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="accuracy")
fig.savefig("accuracy_{}.png".format(model.name))


# confusion matrix
Y_predict = model.predict(X_test, batch_size=128)
Y_predict = np.argmax(Y_predict, axis=1)
Y_true = np.argmax(Y_test, axis=1)

C = np.histogram2d(Y_true, Y_predict, bins=np.linspace(-0.5, 3.5, 5))[0]
Cn = C / np.sum(C, axis=1)
fig = plt.figure()
plt.imshow(Cn, interpolation="nearest", vmin=0, vmax=1, cmap=plt.cm.YlGnBu)
plt.colorbar()
plt.xlabel("predicted mass number")
plt.ylabel("true mass number")
plt.xticks(range(4), species)
plt.yticks(range(4), species)
for x in range(4):
    for y in range(4):
        plt.annotate("%i" % C[x, y], xy=(y, x), ha="center", va="center")
fig.savefig("confusion_{}.png".format(model.name))
