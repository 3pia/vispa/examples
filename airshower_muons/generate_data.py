import numpy as np
from astrotools import auger, coord
from radiotools.atmosphere.models import get_vertical_height


def muonImage(T, xbins=np.linspace(-1E4, 1E4, 21), tbins=None):
    """ Return image of muon events T (x [m], y [m], time [s]) """
    x, y, t = T
    if tbins is None:
        return np.histogram2d(x, y, bins=(xbins, xbins))[0]
    else:
        return np.histogramdd((x, y, t), bins=(xbins, xbins, tbins))[0]


def randMuonNumber(logE, A, nmax=200):
    """ Draw random number of muons """
    scale = 100 * 10**(logE-18) * A**0.15
    n = (0.1 * np.random.randn() + 1) * scale
    return int(min(round(n), nmax))


def randMuonDepth(Xmax, A, n=1):
    """ Draw n random muon depths """
    hmax = get_vertical_height(Xmax)
    hstd = 1400 - (1400 - 400) * (A-1) / 55.
    return np.random.randn(n) * hstd + hmax


def randMuonDirection(n):
    """ Draw n random muon directions from Fischer distribution with concentration parameter kappa ~ 1/sigma^2 """
    return coord.rand_fisher_vec([0, 0, -1], kappa=10, n=n)


# detector
Xground = 1000  # atmospheric overburden, 872.25 g/cm^2 for Auger
hground = get_vertical_height(Xground)  # height corresponding to Xground
spacing = 1000  # m
timeresolution = 25E-9  # 40 MHz
c_light = 3E8

# mockup data
N = 40000
logE = 18
zenith = 0
species = (1, 4, 14, 56)

Adata = np.random.choice(species, N)
Xdata = np.zeros(N)
Tdata = np.zeros((N, 3, 210)) * np.NaN  # nEvents x (x,y,time) x max(nMuons)


for i, A in enumerate(Adata):
    if (i % (N/10) == 0):
        print(i)

    # draw Xmax, require above ground
    Xmax = 1E99
    while Xmax > Xground:
        Xmax = auger.rand_gumbel(logE, A)

    # draw number of muons
    n = randMuonNumber(logE, A)

    # draw production depth for each muon
    h = randMuonDepth(Xmax, A, n)

    # discard muons below ground
    h = h[h > hground]
    n = len(h)

    # draw muon directions
    xdir, ydir, zdir = randMuonDirection(n)

    # calculate propagation time to ground
    t = (h - hground) / (- zdir * c_light)

    # calculate intersection with ground
    xpos = xdir * c_light * t
    ypos = ydir * c_light * t

    # add random offset of +/-0.5 bins to account for random shower core
    xpos += (np.random.rand() - 0.5) * spacing
    ypos += (np.random.rand() - 0.5) * spacing

    # save properties
    Xdata[i] = Xmax
    Tdata[i][0][0:n] = xpos
    Tdata[i][1][0:n] = ypos
    Tdata[i][2][0:n] = t

np.savez_compressed('data-40000.npz', A=Adata, X=Xdata, T=Tdata, species=species)
