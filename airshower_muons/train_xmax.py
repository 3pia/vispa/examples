#!/usr/bin/env pygpu

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tabulate import tabulate

# ---------------------------------------------------------
# Load and preprocess toy data
# ---------------------------------------------------------
data = np.load("/net/scratch/deeplearning/airshower/data.npz")

muons = data["T"]  # arrays of positions (x, y) and times t
xmax = data["X"]  # Xmax [g/cm^2]
species = data["species"]  # simulated mass numbers (1, 4, 14, 56)


def muonImage(muons, xbins, tbins=None):
    """ Create histogram of muon hits for a single event """
    x, y, t = muons
    if tbins is None:
        # 2D histogram of spatial distribution
        return np.histogram2d(x, y, bins=(xbins, xbins))[0]
    else:
        # 3D histogram of spatial + time distribution
        return np.histogramdd((x, y, t), bins=(xbins, xbins, tbins))[0]


# Create 10x10 pixel image of total number of muon hits, no time dimension
npix = 10
xbins = np.linspace(-5000, 5000, npix + 1)  # pixel borders in [m]
image = np.array([muonImage(m, xbins) for m in muons])

# normalize images to hide total number of muons
image /= np.sum(image, axis=(1, 2))[:, np.newaxis, np.newaxis]

# reshape for neural network (add axis of size 1)
image = image.reshape(-1, npix, npix, 1)
xmax = xmax.reshape(-1, 1)

# split train, validation and test samples
i0, i1, i2, i3 = 0, 30000, 35000, 40000
X_train, Y_train = image[i0:i1], xmax[i0:i1]
X_valid, Y_valid = image[i1:i2], xmax[i1:i2]
X_test, Y_test = image[i2:i3], xmax[i2:i3]


# ----------------------------------------------------------
# Define model
# ----------------------------------------------------------
model = tf.keras.models.Sequential(
    [
        tf.keras.layers.Convolution2D(32, (3, 3), padding="valid", activation="relu", input_shape=(npix, npix, 1)),
        tf.keras.layers.Convolution2D(32, (3, 3), padding="valid", activation="relu"),
        tf.keras.layers.Convolution2D(64, (3, 3), padding="valid", activation="relu"),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(256, activation="relu"),
        tf.keras.layers.Dropout(0.3),
        tf.keras.layers.Dense(1),
    ],
    name="airshower_xmax",
)

print(model.summary())


# ----------------------------------------------------------
# Training
# ----------------------------------------------------------
model.compile(
    loss="mse",
    optimizer=tf.keras.optimizers.Adam(lr=1e-3),
    metrics=["mean_absolute_error"],
)

batch_size = 128
result = model.fit(
    X_train,
    Y_train,
    batch_size=batch_size,
    epochs=10,
    verbose=1,
    validation_data=(X_valid, Y_valid),
    callbacks=[tf.keras.callbacks.CSVLogger("history_{}.csv".format(model.name))],
)

model.save("model_{}.h5".format(model.name))


# ----------------------------------------------------------
# Evaluation and Plots
# ----------------------------------------------------------
# evaluate performance
print("Model performance :")
headers = ["", "Loss", "Accuracy"]

table = [
    ["Train", *model.evaluate(X_train, Y_train, verbose=0, batch_size=128)],
    ["Validation", *model.evaluate(X_valid, Y_valid, verbose=0, batch_size=128)],
    ["Test", *model.evaluate(X_test, Y_test, verbose=0, batch_size=128)],
]

print(tabulate(table, headers=headers, tablefmt="orgtbl"))


# training curves
history = np.genfromtxt("history_{}.csv".format(model.name), delimiter=",", names=True)

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["loss"], label="training")
ax.plot(history["epoch"], history["val_loss"], label="validation")
ax.set_yscale("log", nonposy="clip")
ax.legend()
ax.set(xlabel="epoch", ylabel="loss")
fig.savefig("loss_{}.png".format(model.name))

fig, ax = plt.subplots(1)
ax.plot(history["epoch"], history["mean_absolute_error"], label="training")
ax.plot(history["epoch"], history["val_mean_absolute_error"], label="validation")
ax.legend()
ax.set(xlabel="epoch", ylabel="accuracy")
fig.savefig("mean_absolute_error_{}.png".format(model.name))


# predict Xmax
Xmax_train = model.predict(X_train, verbose=0, batch_size=batch_size)
Xmax_valid = model.predict(X_valid, verbose=0, batch_size=batch_size)
Xmax_test = model.predict(X_test, verbose=0, batch_size=batch_size)

# difference to true Xmax
dXmax_train = Xmax_train - Y_train
dXmax_valid = Xmax_valid - Y_valid
dXmax_test = Xmax_test - Y_test

mean_train, std_train = np.mean(dXmax_train), np.std(dXmax_train)
mean_valid, std_valid = np.mean(dXmax_valid), np.std(dXmax_valid)
mean_test, std_test = np.mean(dXmax_test), np.std(dXmax_test)

print("Model performance (dX = predicted - true Xmax)")
headers = ["", "Mean", "Std. deviation"]

table = [
    ["Train", mean_train, std_train],
    ["Validation", mean_valid, std_valid],
    ["Test", mean_test, std_test],
]

print(tabulate(table, headers=headers, tablefmt="orgtbl"))


# scatter
fig, ax = plt.subplots(1)
label = "%s\n$\mu=%.1f, \sigma=%.1f$"
ax.scatter(Y_train, Xmax_train, label=label % ("training set", mean_train, std_train))
ax.scatter(Y_valid, Xmax_valid, label=label % ("validation set", mean_valid, std_valid))
ax.scatter(Y_test, Xmax_test, label=label % ("test set", mean_test, std_test))
ax.plot((600, 1000), (600, 1000), "k--")
ax.legend()
ax.set_xlabel("true $X_\mathrm{max}$ [g/cm$^2$]")
ax.set_ylabel("predicted $X_\mathrm{max}$ [g/cm$^2$]")
ax.grid()
ax.set_aspect("equal")
fig.savefig("scatter_{}.png".format(model.name))
